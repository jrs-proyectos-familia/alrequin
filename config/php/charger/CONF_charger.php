<?php
	namespace config\php\charger;

	require_once('config/php/exceptions/exce_CONF_charger.php');
	use config\php\exceptions\ChargerException;
	use const config\php\args\USE_ORM; // Para monitorear si se desea usar el ORM Doctrine

	const DS = DIRECTORY_SEPARATOR;

	class ConfigCharger {
		public function __construct () {
			require_once('vendor/autoload.php');
			$this -> args();
			$this -> headers();
			$this -> templates();
			$this -> exceptions();
			$this -> db();
			$this -> auth();
			$this -> lang();
			$this -> route();
			AppCharger::routes();
		}

		private function args () {
			require_once('config'.DS.'php'.DS.'args'.DS.'CONF_args.php');
		}

		private function headers () {
			require_once('config'.DS.'php'.DS.'headers'.DS.'CONF_headers.php');
		}

		private function templates () {
			require_once('config'.DS.'php'.DS.'templates'.DS.'tpl_CONF_res_code.php');
			require_once('config'.DS.'php'.DS.'templates'.DS.'tpl_CONF_exceptions.php');
		}

		private function exceptions () {
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'EXCE_index_fatal.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'EXCE_index_catch.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'EXCE_actions.php');

			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_CONF_fatal_error.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_CONF_lang.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_CONF_call_middleware.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_CONF_res_code.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_CONF_redirect.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_CONF_route.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_CONF_db.php');
		
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_data.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_rules.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_auth.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_files.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_annotations_reader.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_annotations_rules_treat.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_annotations_rules_val.php');
			require_once('config'.DS.'php'.DS.'exceptions'.DS.'exce_APP_data_manage.php');
		}
		
		private function db () {
			if (!empty(USE_ORM))
				require_once('config'.DS.'php'.DS.'db'.DS.'db_CONF_doctrine.php');
			else
				require_once('config'.DS.'php'.DS.'db'.DS.'CONF_db.php');
		}
		
		private function auth () {
			require_once('config'.DS.'php'.DS.'auth'.DS.'CONF_auth.php');
		}
		
		private function lang () {
			require_once('config'.DS.'php'.DS.'lang'.DS.'CONF_lang.php');
		}
		
		private function route () {
			require_once('config'.DS.'php'.DS.'route'.DS.'route_CONF_redirect.php');
			require_once('config'.DS.'php'.DS.'route'.DS.'route_CONF_res_code.php');
			require_once('config'.DS.'php'.DS.'route'.DS.'route_CONF_render.php');
			require_once('config'.DS.'php'.DS.'route'.DS.'route_CONF_route_tools.php');
			require_once('config'.DS.'php'.DS.'route'.DS.'route_CONF_call_middleware.php');
			require_once('config'.DS.'php'.DS.'route'.DS.'CONF_route.php');
		}
	}

	class AppCharger {
		public function  __construct () {
			$this -> tools();
			$this -> ini();
			$this -> mixins();
			$this -> middleware();
		}

		private function ini () {
			// ANNOTATIONS
			require_once('app'.DS.'server'.DS.'php'.DS.'ini'.DS.'annotations'.DS.'INI_annotations.php');
			require_once('app'.DS.'server'.DS.'php'.DS.'ini'.DS.'annotations'.DS.'ann_INI_rulesTreat.php');
			require_once('app'.DS.'server'.DS.'php'.DS.'ini'.DS.'annotations'.DS.'ann_INI_rulesVal.php');

			// MWT
			require_once('app'.DS.'server'.DS.'php'.DS.'ini'.DS.'mwtools'.DS.'mwt_INI_rules.php');
			require_once('app'.DS.'server'.DS.'php'.DS.'ini'.DS.'mwtools'.DS.'mwt_INI_treatment.php');
			require_once('app'.DS.'server'.DS.'php'.DS.'ini'.DS.'mwtools'.DS.'mwt_INI_validate.php');
		}

		private function middleware () {
			require_once('app'.DS.'server'.DS.'php'.DS.'middleware'.DS.'MW_authMiddleware.php');
		}

		private function mixins () {
			require_once('app'.DS.'server'.DS.'php'.DS.'mixins'.DS.'MIX_treat.php');
			require_once('app'.DS.'server'.DS.'php'.DS.'mixins'.DS.'MIX_val.php');
		}
		
		private function tools () {
			require_once('app'.DS.'server'.DS.'php'.DS.'tools'.DS.'TOOL_data_manage.php');
			require_once('app'.DS.'server'.DS.'php'.DS.'tools'.DS.'TOOL_files.php');
			require_once('app'.DS.'server'.DS.'php'.DS.'tools'.DS.'TOOL_text.php');
		}

		public static function routes () {
			$baseDir = 'app'.DS.'server'.DS.'php'.DS.'modules';

			$modules = scandir($baseDir);

			if (count($modules)) {
				foreach($modules as $module) {
					if ($module !== '.' && $module !== '..') {
						if (is_file($baseDir.DS.$module.DS.'MOD_'.$module.'.php'))
							require_once($baseDir.DS.$module.DS.'MOD_'.$module.'.php');
					}
				}
			}
		}

		public static function modules ($phpArray, $baseDir) {
			// print_r($phpArray);

			$phpArrayCopy = $phpArray;

			$module = array_splice($phpArrayCopy, 0, 1)[0];
			$className = array_pop($phpArrayCopy);
			
			if (count($phpArrayCopy)) {
				$modDir = array_splice($phpArrayCopy, 0, 1)[0];

				switch ($modDir) {
					case 'crud':
						if (count($phpArrayCopy) === 0) {
							$file = $baseDir.$module.DS.$modDir.DS.strtoupper($className).'_'.$module.'.php';

							if (!is_file($file))
								throw new ChargerException('CHE-100', $file, "Couldn't charge the file");
							
							
							require_once($file);
						} else {
							$sqlDir = array_splice($phpArrayCopy, 0, 1)[0];
							
							if (count($phpArrayCopy) === 0) {
								$file = $baseDir.$module.DS.$modDir.DS.$sqlDir.DS.'SQL_'.$module.'.php';
								
								if (!is_file($file))
									throw new ChargerException('CHE-100', $file, "Couldn't charge the file");
								
								require_once($file);
							}
						}
						break;
					case 'data':
						if (count($phpArrayCopy) === 0) {
							$file = $baseDir.$module.DS.$modDir.DS.'DATA_'.$module.'.php';
							
							if (!is_file($file))
								throw new ChargerException('CHE-100', $file, "Couldn't charge the file");

							require_once($file);
						}
						break;
					case 'middlewaresss':
					// TODO: Revisar que recurso usa esto... En caso de que nadie lo use, entonces eliminarlo
						$actionMidd = array_splice($phpArrayCopy, 0, 1)[0];
						
						if (count($phpArrayCopy) === 0) {
							$file = $baseDir.$module.DS.$modDir.DS.$actionMidd.DS.'MW_'.$actionMidd.'.php';
							
							if (!is_file($file))
								throw new ChargerException('CHE-100', $file, "Couldn't charge the file");

							require_once($file);
						}
						break;
					case 'rules':
						$description = array_splice($phpArrayCopy, 0, 1)[0];
						$typeRule = explode('_', $className)[1];
						
						if (count($phpArrayCopy) === 0) {
							$file = $baseDir.$module.DS.$modDir.DS.$description.DS.$module.'_RULE_'.$typeRule.'_'.$description.'.php';
							
							if (!is_file($file))
								throw new ChargerException('CHE-100', $file, "Couldn't charge the file");
	
							require_once($file);
						}
						break;
					case 'middleware':
						$tvsMethod = array_splice($phpArrayCopy, 0, 1)[0];
						$tvsDescription = array_splice($phpArrayCopy, 0, 1)[0];
						$tvsAction = explode('_', $className)[1];
						
						if (count($phpArrayCopy) === 0 && !empty($tvsAction)) {
							$file = $baseDir.$module.DS.$modDir.DS.$tvsMethod.DS.$tvsDescription.DS.$tvsAction.'_'.$tvsDescription.'.php';
							
							if (!is_file($file))
								throw new ChargerException('CHE-100', $file, "Couldn't charge the file");
							
							require_once($file);
						}
						break;

					default: 
						throw new ChargerException('CHE-000', $baseDir, "An error when try it to charger the file");
						break;
				}
			}
		}
	}

	spl_autoload_register(function ($namespace) {
		$parseNamespace = str_replace('\\', DS, $namespace);

		// Filtrando a app/server/php/
		if (preg_match('/^app\\'.DS.'server\\'.DS.'php\\'.DS.'/', $parseNamespace)) {
			$appArray = explode(DS, $parseNamespace);
			
			$appDir = $appArray[3];
			
			$appChargerArray = array_slice($appArray, 4);
			// $appBaseDir = dirname(__DIR__).DS.'..'.DS.implode( DS, array_slice($appArray, 0, 3)).DS;
			$appBaseDir = implode( DS, array_slice($appArray, 0, 4)).DS;
			
			if ($appDir === 'modules')
				AppCharger::modules($appChargerArray, $appBaseDir);
		}
		
	});
