<?php
	namespace config\php\templates;

	class Exceptions {
		/**
		 * Método que construye el template para mostrar los errores generados dentro del sistema/aplicación
		 *
		 * @param string $errorTitle Titulo del Error
		 * @param string $code Código del Error
		 * @param string $file Archivo dónde se encuentra el Error
		 * @param string $line Linea dónde se encuentra el Error
		 * @param string $message Mensaje representativo del Error
		 * @return string Template del error en HTML
		 */
		public static function errorException (string $errorTitle, string $code, string $file, string $line, string $message) {
			return <<<ERROR
			<div style="
				padding: 5px 10px;
				background-color: #df2d2d;
				margin-bottom: 10px;
				">
				
				<h4 style="
					margin-top: 5px;
					padding-bottom: 5px;
					border-bottom: 1px dashed white;	
					color: white;
					">
					$errorTitle [$code]
				</h4>
				
				<div style="
					display: flex;
					flex-direction: column;
					">

					<span style="
						color: white;
						font-size: 11px;
						text-align: right;
						flex: auto;
						">
						$file <b>:File</b>
					</span>

					<span style="
						color: white;
						font-size: 11px;
						text-align: right;
						flex: auto;
						">
						$line <b>:Line</b>
					</span>
				
				</div>
				
				<p style="
					width: 100%;
					color: white;
					font-size: 14px; 
					">
					$message
				</p>
				
			</div>
			ERROR;
		}
	}
