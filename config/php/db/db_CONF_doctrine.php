<?php
namespace config\php\db;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

use const config\php\args\{MODE, DB_HOST, DB_NAME, DB_USER, DB_PASS, ORM_DRIVER};

class DoctrineORM {

	private static $instance = NULL;

	public static function init () {
		if (is_null(self::$instance))
			self::$instance = new DoctrineORM();

		return self::$instance;
	}
	
	private $isDevMode;
	private $proxyDir = null;
	private $cache = null;
	private $useSimpleAnnotationReader = false;

	private $DB = array(
		'driver' => NULL,
		'host' => NULL,
		'user' => NULL,
		'password' => NULL,
		'dbname' => NULL,
		'charset' => 'UTF8'
	);
	
	private $entityManager = NULL;
	
	const DS = DIRECTORY_SEPARATOR;
	
	/**
	 * Configuración básica de Doctrine ORM
	 */
	private function __construct () {
		if (MODE === 'dev') $this -> isDevMode = true;
		else $this -> isDevMode = false;

		$metaConfig = Setup::createAnnotationMetadataConfiguration(
			$this -> entityPaths(), 
			$this -> isDevMode, 
			$this -> proxyDir, 
			$this -> cache, 
			$this -> useSimpleAnnotationReader
		);

		$this -> DB['driver'] = ORM_DRIVER;
		$this -> DB['host'] = DB_HOST;
		$this -> DB['user'] = DB_USER;
		$this -> DB['password'] = DB_PASS;
		$this -> DB['dbname'] = DB_NAME;


		$this -> entityManager = EntityManager::create(
			$this -> DB,
			$metaConfig
		);
	}

	/**
	 * Método que obtiene todas las entity paths (rutas de entidad) desplegadas a lo largo de los diferentes modulos creados
	 *
	 * @return array arreglo de las distitnas paths de entidades
	 */
	private function entityPaths () {
		$entityPaths = array ();
		
		$modulesPath = 'app'.self::DS.'server'.self::DS.'php'.self::DS.'modules';

		if (is_dir($modulesPath)) {
			$modulesContent = scandir($modulesPath);

			foreach ($modulesContent as $module) {
				if ($module !== '.' && $module !== '..') {
					$entityPath = $modulesPath.self::DS.$module.self::DS.'entity';
					
					if (is_dir($entityPath))
						array_push($entityPaths, $entityPath);
				}
			}

		}	else throw new \Exception ("No se encontró la ruta de los módulos $modulesPath");

		return $entityPaths;
	}

	/**
	 * Método que devuelve el entity manager usado para las operaciones CRUD
	 *
	 * @return entityManager
	 */
	public function getEntityManager () {
		return $this -> entityManager;
	}
}
