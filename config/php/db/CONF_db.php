<?php
namespace config\php\db;

use config\php\exceptions\DbException;

use const config\php\args\{
	DB_HOST,
	DB_NAME,
	DB_USER,
	DB_PASS
};

class DB {
	private static $instace = NULL;

	public static function init () {
		if (is_null(self::$instace))
			self::$instace = new DB();

		return self::$instace;
	}

	public static function destroy () {
		self::$instace = NULL;
	}

	private $conn = NULL;

	private function __construct () {
		$this -> conn = NULL;

		$this -> conn = new \PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
		$this -> conn -> setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$this -> conn -> exec('SET CHARACTER SET utf8');
	}

	public function conn () {
		if (!is_null($this -> conn))
			return $this -> conn;
		else throw new DbException("DBE-100", "Can't get connection");
	}
}
