<?php
namespace config\php\auth;

use Firebase\JWT\JWT;

use config\php\db\DB;
use config\php\exceptions\AuthException;
use const config\php\args\JWT_KEY;

class Auth {

	private static $encrypt = array('HS256');

	public static function generate($user) {
		$time = time();
		
		$token = array(
			'iat' => $time,
			'exp' => $time + (60*60), // 1 Hora para que expire el Token
			// 'exp' => $time--, // 1 Hora para que expire el Token
			'aud' => self::Aud(),
			'user' => array(
				'id' => $user -> get_id(),
				'name' => $user -> get_name(),
				'surname' => $user -> get_surname(),
				'second_surname' => $user -> get_second_surname(),
				'alias' => $user -> get_alias()
			)
		);

		try {
			$encode = JWT::encode($token, JWT_KEY);
		} catch (\Exception $e) {
			throw new AuthException("ATE-100", "Couldn't generate token");
		}
		return $encode;
	}

	private static function getData ($token) {
		$decode = NULL;
		
		try {
			$decode = JWT::decode($token, JWT_KEY, self::$encrypt);
		} catch (\Exception $e) {
			throw new AuthException("ATE-200", "Couldn't get token");
		}

		return $decode;
	} 

	public static function check($token) {
		$success = false;
		$userToken = NULL;
		
		if (isset($token)) {
			$tokenData = self::getData($token);
			
			if ($tokenData !== NULL) {
				if ($tokenData -> aud === self::Aud()) {
					//TODO: Reemplazar esto para doctrine
					$conn = DB::init() -> conn();
					
					if (isset($conn)) {
						// TODO: Extraer esta parte para hacerlo más flexible
						$sql = 'SELECT id FROM users WHERE 
							id = :id AND
							alias = _utf8 :alias COLLATE utf8_bin AND 
							name = :name AND 
							surname = :surname AND 
							second_surname = :secondSurname';
						
						$stmt = $conn -> prepare($sql);
						$stmt -> bindValue('id', $tokenData -> user -> id);
						$stmt -> bindValue('name', $tokenData -> user -> name);
						$stmt -> bindValue('surname', $tokenData -> user -> surname);
						$stmt -> bindValue('secondSurname', $tokenData -> user -> second_surname);
						$stmt -> bindValue('alias', $tokenData -> user -> alias);
						
						$stmt -> execute();
						$row = $stmt -> fetchAll();
						
						if (count($row) === 1) {
							$success = true;
							$userToken = $tokenData -> user;
						}
					}
				}
			}
			
		}

		return array('status' => $success, 'userToken' => $userToken);
	}

	private static function Aud()
	//  CREDITOS A  Rodríguez Patiño, Eduardo (https://anexsoft.com/implementacion-de-json-web-token-con-php)
	{
		$aud = '';

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$aud = $_SERVER['HTTP_CLIENT_IP'];
		} else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$aud = $_SERVER['REMOTE_ADDR'];
		}

		$aud .= @$_SERVER['HTTP_USER_AGENT'];
		$aud .= gethostname();

		return sha1($aud);
	}

}
