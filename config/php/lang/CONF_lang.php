<?php
namespace config\php\lang;

use config\php\exceptions\LangException;
use const config\php\args\{DIR_LANG_CONFIG, DIR_LANG_APP, AVAILABLE_LANG, LANG as LANG_DEFAULT};

//TODO: Hacer un singleton para que no carge el archivo lang cada vez que se requiera.

class Lang {

	const DS = DIRECTORY_SEPARATOR;

	public static function getTextLang (string $key, array $lang) {
		if (array_key_exists($key, $lang)) return $lang[$key];
		
		return NULL;
	}

	/**
	 * Método que recupera el idioma requerido del paquete de idiomas
	 *
	 * @param string $folder Nombre del folder del recurso particular
	 * @param boolean $isLangConfig Usar el directorio de idiomas de la configuración???
	 * @param string $lang Especificar manualmente el idioma del recurso a usar
	 * @return object Objeto con el paquete total del idioma requerido
	 */
	public static function getLang (string $folder, bool $isLangConfig = false, string $lang = NULL) {	
		if ($isLangConfig)
			$route = DIR_LANG_CONFIG;
		else $route = DIR_LANG_APP;

		$fileArray = explode('_', $folder);
		array_shift($fileArray);

		$route .= $folder.self::DS;
		foreach ($fileArray as $part)
			$route .= $part."_";

		if (is_null($lang)) {
			if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
				$langs = LangConfig::prefered_language(AVAILABLE_LANG, $_SERVER['HTTP_ACCEPT_LANGUAGE']);

				if (count($langs)) {
					$langsKeys = array_keys($langs);

					$lang = array_shift($langsKeys);
				} else $lang = LANG_DEFAULT;

			} else $lang = LANG_DEFAULT;
		} else {
			if (!in_array($lang, AVAILABLE_LANG)) $lang = LANG_DEFAULT;
		}

		$route .= strtoupper($lang).'.json';
	
		if (is_file($route)) {
			$content = file_get_contents($route);
			
			$jsonFile = json_decode($content, true);
	
			return $jsonFile;
		} else throw new LangException("LNE-100", "It couldn't get the file");
	
	}

	public static function prefered_language(array $available_languages, $http_accept_language) {
		// CREDITOS a Diego Lárazo (https://diego.com.es/http-language-detection-en-php)
		
		$available_languages = array_flip($available_languages);

		$langs = [];

		preg_match_all('~([\w-]+)(?:[^,\d]+([\d.]+))?~', strtolower($http_accept_language), $matches, PREG_SET_ORDER);

		foreach($matches as $match) {
			list($a, $b) = explode('-', $match[1]) + array('', '');
			$value = isset($match[2]) ? (float) $match[2] : 1.0;
			
			if(isset($available_languages[$match[1]])) {
				$langs[$match[1]] = $value;
				continue;
			}

			if(isset($available_languages[$a])) {
				$langs[$a] = $value - 0.1;
			}

		}

		arsort($langs);
		return $langs;
	}
}
