<?php
namespace config\php\headers;

use const config\php\args\{MODE, PORT_DEV};

if (MODE === 'dev')
	header('Access-Control-Allow-Origin: http://localhost:'.PORT_DEV);
else 
	header('Access-Control-Allow-Origin: '.$_ENV['HOST']);
		
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Allow: GET, POST, PUT, DELETE');
// header('Access-Control-Request-Headers: *');
// header('Acess-Control-Allow-Credentials: true');

