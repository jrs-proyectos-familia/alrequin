<?php
namespace config\php\args;

use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load('config/.env');

class ArgsSystem {
	public function __construct() {
		$this -> state_project();
		$this -> session();
		$this -> data_base();
		$this -> language();
		$this -> directories();
	}

	public function state_project () {
		define(__NAMESPACE__.'\MODE', $_ENV['MODE']);
		define(__NAMESPACE__.'\PORT_DEV', $_ENV['PORT_FRONTEND']);
	}

	public function session () {
		define(__NAMESPACE__.'\JWT_KEY', $_ENV['JWT_KEY']);
	}

	public function data_base () {
		define(__NAMESPACE__ .'\HOST', $_ENV['HOST']);

		define(__NAMESPACE__ .'\DB_HOST', $_ENV['DB_HOST']);
		define(__NAMESPACE__ .'\DB_NAME', $_ENV['DB_NAME']);
		define(__NAMESPACE__ .'\DB_USER', $_ENV['DB_USER']);
		define(__NAMESPACE__ .'\DB_PASS', $_ENV['DB_PASS']);

		define(__NAMESPACE__.'\USE_ORM', $_ENV['USE_ORM']);
		define(__NAMESPACE__.'\ORM_DRIVER', $_ENV['ORM_DRIVER']);
	}

	public function language () {
		define(__NAMESPACE__.'\LANG', $_ENV['LANGUAGE']);
		define(__NAMESPACE__.'\AVAILABLE_LANG', array('es', 'en'));
	}

	public function directories () {
		define(__NAMESPACE__.'\CHMOD_CLIENT', 0755);
		
		define(__NAMESPACE__.'\DIR_PUBLIC', 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR);
		define(__NAMESPACE__.'\DIR_SERVER', 'app'.DIRECTORY_SEPARATOR.'server'.DIRECTORY_SEPARATOR);
		define(__NAMESPACE__.'\DIR_STORE', 'app'.DIRECTORY_SEPARATOR.'store'.DIRECTORY_SEPARATOR);
		define(__NAMESPACE__.'\DIR_LANG_CONFIG', 'config'.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR);
		define(__NAMESPACE__.'\DIR_LANG_APP', 'app'.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR);
	}
}

class ArgsApp {
	public function __construct () {
		$this -> routes();
	}

	public function routes () {
		define(__NAMESPACE__.'\ROUTES', [
			"HOME" => 'inicio',
			"ERROR_404" => 'error/404',
			"USERS" => [
				"MAIN" => 'usuario',
				"EDIT_NAME" => 'usuario/editar/nombre',
				"EDIT_ALIAS" => 'usuario/editar/alias',
				"EDIT_IMAGE" => 'usuario/editar/imagen'
			],
			"LOGIN" => [
				"MAIN" => 'login',
				"CHECK" => 'login/check'
			]
		]);
	}
}

new ArgsSystem();
new ArgsApp();
