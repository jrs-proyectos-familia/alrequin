<?php
namespace config\php\route;

use config\php\route\RouteTools;
use config\php\route\Redirect;
use config\php\lang\Lang;
use config\php\route\{CallMiddleware, MemoryMiddleware};
use config\php\exceptions\RouteException;
use config\php\route\ResCode;
use function config\php\route\getHeaders;
use const config\php\args\{LANG as LANG_DEFAULT, AVAILABLE_LANG, ROUTES};

class Route extends RouteTools {
	private static $routes = array(
		'GET' => array(),
		'POST' => array(),
		'PUT' => array(),
		'DELETE' => array()
	);
	private static $firstReq = array(
		'route' => null,
		'middlewares' => null,
		'view' => null,
		'method' => null,
		'params' => null,
		'data' => null,
		'files' => null,
		'lang' => null,
		'auth' => null
	);

	/**
	 * Resetea la ruta a lanzar
	 *
	 * @return void
	 */
	private static function clearFirstReq () {
		foreach(self::$firstReq as $key => $value)
			self::$firstReq[$key] = null;
	}
	/**
	 * Valida el número de partes por las cuales estan conformadas las rutas [del browser y las guardadas por el usuario]
	 * eje: 
	 * 	browser => usuario/perfil/joaquin-reyes-sanchez => ['usuario','perfil', 'joaquin-reyes-sanchez] = 3 partes
	 * 	guardadas => usuario/perfil/ultimo/joaquin-reyes-sanchez => ['usuario', 'perfil', 'ultimo', 'joaquin-reyes-sanchez'] = 4 partes
	 *
	 * @param [array] $partsBrowser
	 * @param [array] $partsRoute
	 * @return boolean 
	 */
	private static function valNumberOfParts($partsBrowser, $partsRoute) {
		if (count($partsBrowser) === count($partsRoute)) return true;
		return false;
	}

	/**
	 * Valida que las rutas guardadas por el usuario no sean todas completamente ${params}
	 * eje:
	 * 	guardadas => ${param}/${param}/${param} = false (Al menos una parte debe de ser diferente a ${param)
	 *
	 * @param [array] $partsBrowser
	 * @param [array] $partsRoute
	 * @return boolean
	 */
	private static function valNumberOfParams($partsBrowser, $partsRoute) {
		$numberParams = 0;
		
		foreach($partsRoute as $part)
			if ($part === '${param}') $numberParams++;

		if ($numberParams === count($partsBrowser)) return false;
		return true;
	}

	/**
	 * Valida que las partes de la ruta guardada coincidan con la ruta pedida por el Browser
	 *
	 * @param [array] $partsBrowser
	 * @param [array] $partsRoute
	 * @param [array] $routeSaved => Contiene todos los elementos de la ruta guardada [route, middlewares, method]
	 * @return boolean
	 */
	private static function valEqualParts ($partsBrowser, $partsRoute, $routeSaved) {
		self::clearFirstReq();

		$success = 0;
		$arrayParams = array();
		// ['a', '${param}', 'b'] Guardada
		// ['a', 'hola', 'b'] Browser
		foreach($partsBrowser as $index => $partBrowser) {
			if ($partBrowser === $partsRoute[$index]) $success++;
			elseif ($partsRoute[$index] === '${param}') {
				array_push($arrayParams, $partsBrowser[$index]);
				$success++;
			}
		}
		
		if ($success == count($partsBrowser)) {
			self::$firstReq['route'] = $routeSaved['route'];
			self::$firstReq['middlewares'] = $routeSaved['middlewares'];
			self::$firstReq['method'] = $routeSaved['method'];
			self::$firstReq['view'] = $routeSaved['view'];
			self::$firstReq['params'] = $arrayParams;

			return true;
		}
		return false;
	}

	private static function valMethod() {
		$success = false;
		
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'GET':
				// Solo para vistas sin formularios
				if (self::$firstReq['method'] === 'get') {
					$success = true;

					self::$firstReq['data'] = isset($_GET['data']) ? $_GET['data']: NULL;
				}
				break;
			case 'POST':
				// Aqui van a caer todos los formularios JSON y los Multipart ya sean para crear o actualizar
				
				if (self::$firstReq['method'] === 'post') {
					$Content = "";
					// Esto es debido a que Fetch convierte todas las cabeceras "Headers" en lowercase
					$HEADERS = getHeaders();
					
					foreach ($HEADERS as $header => $value) {
						if (strtolower($header) === 'content-type') {
							$Content = $value;
							break;
						}
					}

					if ($Content === 'application/json') {
						$success = true;
						
						$data = json_decode(file_get_contents('php://input'), true);
						
						self::$firstReq['data'] = $data;
					} else {
						$Accept = "";

						$HEADERS = getHeaders();
					
						foreach ($HEADERS as $header => $value) {
							if (strtolower($header) === 'accept') {
								$Accept = $value;
								break;
							}
						}

						if ($Accept === 'multipart/form-data') {
							$success = true;
	
							self::$firstReq['data'] = $_POST;
							
							if (isset($_FILES) && count($_FILES) > 0)
								self::$firstReq['files'] = $_FILES;
						}
					}
				}
				break;
			case 'PUT':
				// Solo actualizaciones de datos "formularios de actualización de datos" (A excepción de las actualizaciones que contengan archivos, estas ultimas se van a POST)
				if (self::$firstReq['method'] === 'put') {
					$Content = "";
					$HEADERS = getHeaders();
					
					foreach ($HEADERS as $header => $value) {
						if (strtolower($header) === 'content-type') {
							$Content = $value;
							break;
						}
					}

					if ($Content === 'application/json') {
						$success = true;
	
						$data = json_decode(file_get_contents('php://input'), true);
	
						self::$firstReq['data'] = $data;
					}
				}
				break;
			case 'DELETE':
				// Para eliminar datos específicos
				if (self::$firstReq['method'] === 'delete') {
					$Content = "";
					$HEADERS = getHeaders();
					
					foreach ($HEADERS as $header => $value) {
						if (strtolower($header) === 'content-type') {
							$Content = $value;
							break;
						}
					}
					
					if ($Content === 'application/json') {
						$success = true;
						
						$data = json_decode(file_get_contents('php://input'), true);
	
						self::$firstReq['data'] = $data;
					}
				}
				break;
			default:
				self::clearFirstReq();
				break;
		}

		return $success;
	}

	private static function getSessionToken () {
		$HEADERS = getHeaders();

		foreach ($HEADERS as $header => $value) {
			if (strtolower($header) === 'authorization') {
				$rawAuth = explode(' ', $value);
				
				if (strtolower($rawAuth[0]) === 'token')
					self::$firstReq['auth'] = $rawAuth[1];
				
				break;
			}
		}

	}

	private static function getLang () {
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			$langs = Lang::prefered_language(AVAILABLE_LANG, $_SERVER['HTTP_ACCEPT_LANGUAGE']);

			if (count($langs)) {
				$langsKeys = array_keys($langs);

				self::$firstReq['lang'] = array_shift($langsKeys);
			} else {
				// Aqui se podría restringir la entrada a la web debido al idioma o en su caso redirigirlo a otro sitio... Esto dependerá del desarrollador y proyecto
				self::$firstReq['lang'] = LANG_DEFAULT;
			}
		} else self::$firstReq['lang'] = LANG_DEFAULT;
	}

	public static function lauchRoute ($browserRoute) {
		$success = false;
		
		$parseBrowserRoute = self::parseURL($browserRoute);
		
		if (array_key_exists($_SERVER['REQUEST_METHOD'], self::$routes)) {
			$routesMethod = self::$routes[$_SERVER['REQUEST_METHOD']];
	
			foreach ($routesMethod as $route) {
				$parseRoute = self::parseURL($route['route']);
				
				if (self::valNumberOfParts($parseBrowserRoute, $parseRoute)) {
					if (self::valNumberOfParams($parseBrowserRoute, $parseRoute))
						if (self::valEqualParts($parseBrowserRoute, $parseRoute, $route))
							if (self::valMethod()) {
								self::getSessionToken();
								self::getLang();

								$success = true;

								break;
							}
				}
			}
		} else throw new RouteException("REE-200", "There's not support", [$_SERVER['REQUEST_METHOD']]);
		
		if (!$success) ResCode::init() -> response(404);
		else {
			if (count(self::$firstReq['middlewares'])) {
				MemoryMiddleware::clearInstance();

				new CallMiddleware(self::$firstReq);
			} else throw new RouteException("REE-101", "Need a middleware", [self::$firstReq['route']]);
		}
	}

	/**
	 * Función que construye los atributos escenciales para la validación y lanzamiento de rutas del cliente... Estos a su vez sirve como parametros escenciales para el construir el primer request $firstReq
	 *
	 * @param [string] $route
	 * @param [array functions] $middlewares
	 * @param [string] $method
	 * @param [view] $view
	 * @return array con los objetos a guardar
	 */
	private static function buildingRouteARGS ($route, $middlewares, $method, $view) {
		if (self::validRoute($route)) {
			$routeARGS = array(
				'route' =>  $route,
				'middlewares' => count($middlewares) ? $middlewares : array(),
				'method' =>  $method,
				'view' => $view
			);

			return $routeARGS;
		} else throw new RouteException("REE-100", "Need a correct route");
	}

	public static function routesGET () {
		// ¿Singletón?
		$routesGet = array();

		foreach (self::$routes['GET'] as $route)
			array_push($routesGet, $route['route']);

		return $routesGet;
	}

	public static function get ($route, ...$middlewares) {
		$debug = debug_backtrace();
		$file = explode(DIRECTORY_SEPARATOR, $debug[0]['file']);
		array_pop($file);
		$view = array_pop($file);

		$routeARGS = self::buildingRouteARGS($route, $middlewares, 'get', $view);

		array_push(self::$routes['GET'], $routeARGS);
	}

	public static function post($route, ...$middlewares) {
		$debug = debug_backtrace();
		$file = explode(DIRECTORY_SEPARATOR, $debug[0]['file']);
		array_pop($file);
		$view = array_pop($file);

		$routeARGS = self::buildingRouteARGS($route, $middlewares, 'post', $view);

		array_push(self::$routes['POST'], $routeARGS);
	}

	public static function put($route, ...$middlewares) {
		$debug = debug_backtrace();
		$file = explode(DIRECTORY_SEPARATOR, $debug[0]['file']);
		array_pop($file);
		$view = array_pop($file);

		$routeARGS = self::buildingRouteARGS($route, $middlewares, 'put', $view);

		array_push(self::$routes['PUT'], $routeARGS);
	}

	public static function delete($route, ...$middlewares) {
		$debug = debug_backtrace();
		$file = explode(DIRECTORY_SEPARATOR, $debug[0]['file']);
		array_pop($file);
		$view = array_pop($file);

		$routeARGS = self::buildingRouteARGS($route, $middlewares, 'delete', $view);

		array_push(self::$routes['DELETE'], $routeARGS);
	}
	
}
