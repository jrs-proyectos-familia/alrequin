<?php
namespace config\php\route;

use config\php\exceptions\CallMiddlewareException;
use config\php\route\ResCode;
use function config\php\route\Render;

/**
 * Funciones que funcionan como puente entre el contenido interno de cada middleware y las operaciones de este archivo (Memoria de las rutas y llamadas de los middlewares siguientes)
 *
 */
function getMemory_Req () {
	return MemoryControlReq::init();
}

function getMemory_Next () {
	MemoryControlNext::clearInstance();

	return MemoryControlNext::init();
}

function getMemory_Res () {
	return MemoryControlRes::init();
}

function recursive_setReq ($keys, $value, $req, $newReq = array(), $route = NULL, $c = 0) {
	if (count($keys) === 0) return $value;

	$key = array_shift($keys);
	$route = $route."\\".$key;

	if (array_key_exists($key, $req)) {
		$newReq[$key] = recursive_setReq($keys, $value, $req[$key], array(), $route);
		unset($req[$key]);
	} else throw new CallMiddlewareException("CME-300", "No exists the route for setReq", [$route, "setReq"]);

	if (count($req))
		$newReq = array_merge($newReq, $req);

	return $newReq;
}

function recursive_mergeReq ($merge, $req, $newReq = array(), $c = 0) {
	if (count($merge) === 0) return $newReq;
	
	if (count($merge) > 0) {
		$firstKey_Merge = array_key_first($merge);
		
		if (array_key_exists($firstKey_Merge, $req)) {
			if (!is_array($merge[$firstKey_Merge])) {
				// Siempre será reemplazado por el original
				$newReq[$firstKey_Merge] = $merge[$firstKey_Merge];
			} else {
				if (count($merge[$firstKey_Merge]) === 0)
					$newReq[$firstKey_Merge] = array();
				else
					$newReq[$firstKey_Merge] = recursive_mergeReq($merge[$firstKey_Merge], $req[$firstKey_Merge]);
			}
			
			// unset($req[$firstKey_Merge]);
		} else $newReq[$firstKey_Merge] = $merge[$firstKey_Merge];
	
		unset($merge[$firstKey_Merge]);
	}
	
	return recursive_mergeReq($merge, $req, $newReq, $c = $c + 1);
}

function recursive_toStringReq ($req, $c = 0) {
	// TODO: COMPONER ESTA FUNCION
	if (count($req) === 0) {
		return;
	}

	$firstKey = array_key_first($req);

	if ($c === 0) {
		?>
			<h5><?php echo $firstKey ?></h5>
			<ul>
		<?php
	}

	if (!is_array($req[$firstKey])) {
		if (gettype($req[$firstKey]) === 'object') {
			?>
				<li>
					<small>
						<?php print_r($req[$firstKey])?>
					</small>
				</li>
			<?php
		} else if (empty($req[$firstKey])) {
			?>
				<li>================</li>
			<?php
		} else {
			?>
				<li><?php echo $req[$firstKey]?></li>
			<?php
		}
	} else {
		if (count($req[$firstKey]) === 0) {
			?>
				<li>================</li>
			<?php
		} else recursive_toStringReq($req[$firstKey]);
	}

	unset($req[$firstKey]);
	?>
		</ul>
	<?php
	
	return recursive_toStringReq($req, $c);
}

/**
 * Classe que contiene una pequeña memoria de las propiedades que pasarán de middleware a middleware
 */
class MemoryMiddleware {
	private static $instance = NULL;

	public static function init ($req = NULL) {
		if (self::$instance === NULL)
			self::$instance = new MemoryMiddleware($req);

		return self::$instance;
	}

	public static function clearInstance () {
		self::$instance = NULL;
	}

	private $req = NULL;

	private $memoryReq = NULL;
	private $memoryNext = NULL;
	private $memoryRes = NULL;

	private function __construct($req) {
		$this -> req = $req;
		
		$this -> memoryReq = new \ReflectionFunction('config\php\route\getMemory_Req');
		$this -> memoryNext = new \ReflectionFunction('config\php\route\getMemory_Next');
		$this -> memoryRes = new \ReflectionFunction('config\php\route\getMemory_Res');		
	}
	
	// Solo se podrán usar en la clase CallMiddleware
	protected function get_req () {
		return $this -> req;
	}

	protected function set_req ($req) {
		$this -> req = $req;
	}

	private function val_keys_req ($keys) {
		foreach($keys as $key) {
			$isMiddlewares = $key === 'middlewares';
			$isRoute = $key === 'route';
			$isView = $key === 'view';
			$isMethod = $key === 'method';
			$isParams = $key === 'params';
			$isData = $key === 'data';
			$isFiles = $key === 'files';
			$isAuth = $key === 'auth';
			$isLang = $key === 'lang';

			if ($isMiddlewares) throw new CallMiddlewareException("CME-400", "Can't mix middlewares", ['middlewares', '$req']);
			if ($isRoute) throw new CallMiddlewareException("CME-400", "Can't mix route", ['route', '$req']);
			if ($isView) throw new CallMiddlewareException("CME-400", "Can't mix view", ['view', '$req']);
			if ($isMethod) throw new CallMiddlewareException("CME-400", "Can't mix method", ['method', '$req']);
			if ($isParams) throw new CallMiddlewareException("CME-400", "Can't mix params", ['params', '$req']);
			if ($isData) throw new CallMiddlewareException("CME-400", "Can't mix data", ['data', '$req']);
			if ($isFiles) throw new CallMiddlewareException("CME-400", "Can't mix files", ['files', '$req']);
			if ($isAuth) throw new CallMiddlewareException("CME-400", "Can't mix auth", ['auth', '$req']);
			if ($isLang) throw new CallMiddlewareException("CME-400", "Can't mix lang", ['lang', '$req']);
		}
	}

	protected function set_key_req($route, $value) {
		$this -> val_keys_req(array_slice($route, 0, 1));

		if (array_key_exists($route[0], $this -> req)) {
			$reqCopy[$route[0]] = $this -> req[$route[0]];
			$newReq = recursive_setReq($route, $value, $reqCopy);
			
			$this -> req = array_merge($this -> req, $newReq);
		} else throw new CallMiddlewareException("CME-401", "Couldn't find main key", [$route[0]]);
	}

	protected function set_merge_req ($merge) {
		$merge_keys = array_keys($merge);
		
		$this -> val_keys_req($merge_keys);

		$newReq = recursive_mergeReq($merge, $this -> req);

		$this -> req = array_merge($this -> req, $newReq);
	}

	protected function get_res () {
		return $this -> res;
	}

	protected function get_memory_next () {
		return $this -> memoryNext;
	}

	protected function get_memory_res () {
		return $this -> memoryRes;
	}

	protected function get_memory_req () {
		return $this -> memoryReq;
	}
}

class MemoryControlReq extends MemoryMiddleware {
	private static $instance = NULL;

	public static function init ($req = NULL) { // El parametro $req = NULL es necesario debido a un warning que se genera por ser clases singleton ambas clases
		if (self::$instance === NULL)
			self::$instance = new MemoryControlReq();

		return self::$instance;
	}

	private $memory = NULL;

	private function __construct () {
		$this -> memory = parent::init();
	}

	public function __get($name) {
		if ($name === 'memory') {
			$reqCopy = array_filter($this -> memory -> get_req(), function ($value, $key) {
				return $key !== 'middlewares' && $key !== 'route';
			}, ARRAY_FILTER_USE_BOTH);

			return $reqCopy;
		} else throw new CallMiddlewareException("CME-402", "Couldn't find the \$req param", [$name, '$req']);
	}

	public function __set($name, $value) {
		throw new CallMiddlewareException("CME-200", "Can't overwrite to \$req", ['$req']);
	}

	public function set_key($route = NULL, $value = NULL) {
		if (!empty($route) && !empty($value)) {
			$arrayRoute = explode('\\', $route);
			if (count($arrayRoute))
				$this -> memory -> set_key_req($arrayRoute, $value);
			else throw new CallMiddlewareException("CME-500", "Couldn't get access");
		} else throw new CallMiddlewareException("CME-101", "No empty data");
	}

	public function set_merge($merge) {
		if (is_array($merge) && count($merge))
			$this -> memory -> set_merge_req($merge);
		else throw new CallMiddlewareException("CME-100", "Only can use array", ['$merge[array]']);
	}

	public function toString($mainKey = NULL) {
		$reqCopy = array_filter($this -> memory -> get_req(), function ($value, $key) {
			return $key !== 'middlewares' && $key !== 'route';
		}, ARRAY_FILTER_USE_BOTH);

		if (array_key_exists($mainKey, $reqCopy)) {
			?>
				<h4>Mostrando datos de memoria Req [<?php echo $mainKey?>]</h4>
			<?php

			$reqCopy = $reqCopy[$mainKey];
		}
		else {
			?>
				<h4>Mostrando datos de memoria Req [Global]</h4>
			<?php
		}

		?>
			<div style="padding: 0px 20px;">
				<?php
					if (empty($reqCopy)) {
						?> <b>VACIO</b> <?php
					} else recursive_toStringReq($reqCopy);
				?>
			</div>
		<?php
	}

}

class MemoryControlRes {
	private static $instance = NULL;

	public static function init () {
		if (self::$instance === NULL)
			self::$instance = new MemoryControlRes();

		return self::$instance;
	}

	private function __construct(){}

	public function code ($code, $message = []) { // No funciona el type hiting cuando se aplica la reflection
		$isCode = gettype($code) === 'integer';
		$isMessage = gettype($message) === 'array';

		if ($isCode && $isMessage) ResCode::init() -> response($code, $message);
		else throw new CallMiddlewareException('CME-100', "Can't use those data types", ['$code[integer], $message[array]']);
	}

	public function render ($view, $args) {
		$isCode = gettype($view) === 'string';
		$isArgs = gettype($args) === 'array';

		if ($isCode && $isArgs) echo "LLAMAR AL RENDER";
		else throw new CallMiddlewareException('CME-100', "Can't use those data types", ['$view[string], $args[array]']);
	}


	public function __set($name, $value) {
		throw new CallMiddlewareException('CME-200', "Can't overwrite to \$res", ['$res']);
	}
}

class MemoryControlNext {
	private static $instance = NULL;

	public static function init () {
		if (self::$instance === NULL)
			self::$instance = new MemoryControlNext();

		return self::$instance;
	}
	
	public static function clearInstance () {
		self::$instance = NULL;
	}

	private function __construct(){}


	public function __get($name) {
		if ($name === 'mw') {
			new CallMiddleware();
			die(); // Para detener cualquier contenido siguiente a la ejecución de next;
		} else throw new CallMiddlewareException("CME-402", "Couldn't find the \$next param", [$name, '$next']);
	}
}

/**
 * Clase de Control que valida y realiza la primera ejecución del middleware escrito en cada ruta
 */
class CallMiddleware extends MemoryMiddleware { // Se añade como herencia para proteger los métodos protected de las llamadas desde fuera de esta clase
	private $memory = NULL;

	public function __construct ($req = NULL) {
		// Se ejecuta 1 vez para iniciar el camino de los middlewares 
		$this -> memory = parent::init($req);
		
		$req = $this -> memory -> get_req();
		
		if (!empty($req) && is_array($req)) {
			
			$this -> nextMiddleware();
			
		} else throw new CallMiddlewareException("CME-402", "Couldn't find the \$middleware param", ['$req', '$middleware']);
	}

	public function nextMiddleware () {
		$req = $this -> memory -> get_req();
		$memoryReq = $this -> memory -> get_memory_req();
		$memoryRes = $this -> memory -> get_memory_res();
		$memoryNext = $this -> memory -> get_memory_next();
		
		if (count($req) > 0) {
			$midd = NULL;
			
			if (is_callable($req['middlewares'][0])) {
				$midd = new \ReflectionFunction($req['middlewares'][0]);
				
				array_shift($req['middlewares']); // Eliminamos siempre el primer middleware para ir avanzando entre middlewares
				$this -> memory -> set_req($req); // Actualizamos la memoria de los middlewares

				switch (count($midd -> getParameters())) {
					case 2:
						$isReq = $midd -> getParameters()[0] -> getName() === 'req';
						$isRes = $midd -> getParameters()[1] -> getName() === 'res';
						
						if ($isReq && $isRes)
							$midd -> invoke($memoryReq -> invoke(), $memoryRes -> invoke());
						else throw new CallMiddlewareException("CME-600", "\$req, \$res not found", ['$req, $res', $req['route']]);
						break;
					case 3:
						$isReq = $midd -> getParameters()[0] -> getName() === 'req';
						$isRes = $midd -> getParameters()[1] -> getName() === 'res';
						$isNext = $midd -> getParameters()[2] -> getName() === 'next';

						if ($isReq && $isRes && $isNext)
							if (count($req['middlewares']))
								$midd -> invoke($memoryReq -> invoke(), $memoryRes -> invoke(), $memoryNext -> invoke());
							else throw new CallMiddlewareException("CME-602", "\$res not found", [$req['route']]);
						else throw new CallMiddlewareException("CME-600", "\$req, \$res, \$next not found", ['$req, $res, $next', $req['route']]);
						break;
					default: 
						throw new CallMiddlewareException("CME-601", "Should have 2 or 3 params", [$req['route'], "2-3"]);
				}
			} else if (method_exists($req['middlewares'][0], 'middleware')) {
				$midd = new \ReflectionClass($req['middlewares'][0]);

				array_shift($req['middlewares']); // Eliminamos siempre el primer middleware para ir avanzando entre middlewares
				$this -> memory -> set_req($req); // Actualizamos la memoria de los middlewares

				switch (count($midd -> getMethod('middleware') -> getParameters())) {
					case 2:
						$isReq = $midd -> getMethod('middleware') -> getParameters()[0] -> getName() === 'req';
						$isRes = $midd -> getMethod('middleware') -> getParameters()[1] -> getName() === 'res';
						
						if ($isReq && $isRes)
							$midd -> getMethod('middleware') -> invoke($midd -> newInstance(), $memoryReq -> invoke(), $memoryRes -> invoke());
						else throw new CallMiddlewareException("CME-600", "\$req, \$res not found", ['$req, $res', $req['route']]);
						break;
					case 3:
						$isReq = $midd -> getMethod('middleware') -> getParameters()[0] -> getName() === 'req';
						$isRes = $midd -> getMethod('middleware') -> getParameters()[1] -> getName() === 'res';
						$isNext = $midd -> getMethod('middleware') -> getParameters()[2] -> getName() === 'next';

						if ($isReq && $isRes && $isNext)
							if (count($req['middlewares']))
								$midd -> getMethod('middleware') -> invoke($midd -> newInstance(), $memoryReq -> invoke(), $memoryRes -> invoke(), $memoryNext -> invoke());
							else throw new CallMiddlewareException("CME-602", "\$res not found", [$req['route']]);
						else throw new CallMiddlewareException("CME-600", "\$req, \$res, \$next not found", ['$req, $res, $next', $req['route']]);
						break;
					default:
						throw new CallMiddlewareException("CME-601", "Should have 2 or 3 params", [$req['route'], "2-3"]);
				}
			} else throw new CallMiddlewareException("CME-603", "Should be a function or class", [$req['route']]);

		} else throw new CallMiddlewareException("CME-604", "Need a middleware", [$req['route']]);
	}
}
