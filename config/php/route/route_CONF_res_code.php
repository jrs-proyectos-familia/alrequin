<?php
namespace config\php\route;

use config\php\route\Redirect;
use config\php\templates\ResCode as TemplateResCode;
use config\php\exceptions\ResCodeException;
use const config\php\args\MODE;

// use app\server\php\modules\error404\View as ErrorView;

class ResCode {

	private static $instance = NULL;

	public static function init () {
		if (is_null(self::$instance))
			self::$instance = new ResCode();
		
		return self::$instance;
	}
	
	private function __construct () {}

	/**
	 * Método que lanza el proceso de respuesta a una petición ó proceso ocurrida en el servidor
	 *
	 * @param integer $code Código de respuesta
	 * @param array $message Mensaje de salida
	 */
	public function response (int $code, array $message = []) {
		header($this -> res_protocol(). ' '. $code . ' '. $this -> res_codes($code));

		$this -> res_actions($code);
		$this -> res_message($code, $message);

		die();
	} 

	/**
	 * Método para obtener el mensaje correspondiente al código de respuesta ejecutado
	 *
	 * @param integer $code Código de respuesta
	 * @return string $mensaje del código de respuesta
	 */
	private function res_codes (int $code) {
		$text_code = NULL;

		switch ($code) {
			case 200: $text_code = 'OK'; break; // Para cargar vistas
			case 201: $text_code = 'Created'; break; // Para respuestas satisfactorias que hayan creado un recurso
			case 204: $text_code = 'No Content'; break; // Para respuestas satisfactorias POST, PUT, DELETE
			case 302: $text_code = 'Moved Temporarily'; break; // Para mantenimientos en páginas especificas
			case 401: $text_code = 'Unauthorized'; break; // Para intentos fallidos en vistas protegidas
			case 404: $text_code = 'Not Found'; break; // Para vistas no encontradas
			case 406: $text_code = 'Not Acceptable'; break; // Para datos no admitidios por POST, PUT, DELETE
			case 500: $text_code = 'Internal Server Error'; break; // Para Errores del Servidor en trabajos internos (de varios tipos)
			default: exit('Unknown http status code "'. htmlentities($code) .'"');
		}

		return $text_code;
	}

	/**
	 * Método que devuelve el protocolo de salida de la respuesta
	 *
	 * @return string Protocolo de salida
	 */
	private function res_protocol () {
		return (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
	}

	/**
	 * Método que lanza una acción dependiendo del código de salida
	 *
	 * @param integer $code Código de respuesta
	 */
	private function res_actions (int $code) {
		switch ($code) {
			case 404:
				if (MODE === 'dev') print(TemplateResCode::error404_dev());
				else if (MODE === 'prod') {
					echo "REDIRIGIR AL ERROR 404";
					// Redirect::route(ErrorView::route);
				} else throw new ResCodeException("RCE-100", "Error with code 404", [$this -> code]);
				break;
		}
	}

	/**
	 * Método que envia un json de salida dependiendo del código de respuesta
	 *
	 * @param integer $code Código de respuesta
	 * @param array $message Array con llaves el cual se convertirá en el objeto json de salida
	 */
	private function res_message (int $code, array $message) {
		switch ($code) {
			case 201:
			case 406:
			case 500:
				if (count($message))
					echo json_encode($message);
				else throw new ResCodeException("RCE-200", "Need a message for the code", [$this -> code]);
				break;
		}
	}
}

function ResCodesss (int $code, array $message = []) {
	// ESTE SERÁ USADO EN ROUTE
	new ResCodes($code, $message);
}
