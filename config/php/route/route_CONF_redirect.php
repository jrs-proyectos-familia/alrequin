<?php
namespace config\php\route;

use config\php\route\Route;
use config\php\exceptions\RedirectException;
use const config\php\args\HOST;

use app\server\php\tools\Text;

class Redirect {
	public static function route (string $route) {
		if (!empty($route)) {
			if (in_array($route, Route::routesGET())) {
				$currentURL = Text::replaceFirst('/', '', $_SERVER['REQUEST_URI']);
				if ($currentURL !== $route) {
					$ch = curl_init(HOST.$route);
			
					curl_setopt($ch, CURLOPT_HEADER, 0);
			
					curl_exec($ch);
					curl_close($ch);
	
					// header('Location:'.HOST.$route);
	
					die();
				} else throw new RedirectException("RTE-102", "It's de same route");
			} else throw new RedirectException("RTE-101", "Need a GET route");
		} else throw new RedirectException("RTE-100", "Need a route");
	}
}
