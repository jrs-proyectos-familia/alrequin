<?php
namespace config\php\route;

use const config\php\args\DIR_PUBLIC;
use const config\php\args\HOST;
use function config\php\route\ResCode; // TODO: CAMBIAR A CLASE

const DS = DIRECTORY_SEPARATOR;

// TODO: Componer esta parte puesto que ya no existe el archivo _view.php de cada vista
class GetPages {
	private static $instance = NULL;

	public static function instance () {
		if (self::$instance === NULL)
			self::$instance = new GetPages();

		return self::$instance;
	}

	public static function clearInstance () {
		self::$instance = NULL;
	}

	private $pagesARGS = array(
		'pages' => array()
	);

	private $totalViews = 0;

	private function __construct () {
		$viewsDir = 'app'.DS.'server'.DS.'php'.DS.'views';

		$views = scandir($viewsDir);

		if (count($views) > 1) {
			foreach($views as $view) {
				if ($view !== '.' && $view !== '..') {
					$file = $viewsDir.DS.$view.DS.'_view.php';
					
					$this -> totalViews++;

					if (is_file($file)) {
						$fileOpen = fopen($file, 'r');
						
						if ($fileOpen) {
							$buffer = fgets($fileOpen, 4096);

							$textNameAlias = NULL;
							$textRoute = NULL;

							while($buffer !== false) {
								if (preg_match('/"title"/', $buffer)) $textNameAlias = $buffer;
								else if (preg_match("/'title'/", $buffer)) $textNameAlias = $buffer;
								
								if (preg_match('/"mainRoute"/', $buffer)) $textRoute = $buffer;
								else if (preg_match("/'mainRoute'/", $buffer)) $textRoute = $buffer;								
								
								$buffer = fgets($fileOpen, 4096);
							}

							if (!feof($fileOpen))
								throw new \Exception('No se pudo conseguir los parametros para renderizar la vista');

							
							fclose($fileOpen);

							if (!is_null($textNameAlias) && !is_null($textRoute)) {
								$nameAlias = explode('=>', $textNameAlias);
								$replaceNameAlias = str_replace("'", " ", $nameAlias[1]);
								$replace2NameAlias = str_replace('"', " ", $replaceNameAlias);
								$replace3NameAlias = str_replace(',', " ", $replace2NameAlias);

								$route = explode('=>', $textRoute);
								$replaceRoute = str_replace("'", " ", $route[1]);
								$replace2Route = str_replace('"', " ", $replaceRoute);
								$replace3Route = str_replace(',', " ", $replace2Route);
								
								$viewARGS = array(
									$view => array(
										'link' => HOST.trim($replace3Route),
										'router' => strtolower(trim($replace3NameAlias)),
										'alias' => strtolower(trim($replace3NameAlias))
									)
								);
								
								$this -> pagesARGS['pages'] = array_merge($this -> pagesARGS['pages'], $viewARGS);
							}
						}
					}
				}
			}
		}
	}

	public function get_page_args () {
		return $this -> pagesARGS;
	}

	public function get_total_views () {
		return $this -> totalViews;
	}
}

/**
 * Función que se encargará de lanzar la vista al cliente
 *
 * @param [Array Object] $viewARGS Parametros epecíficos y únicos de cada vista
 * @return void
 */
function Render (string $view, array $viewARGS) {
	$loaderTemplates = new \Twig\Loader\FilesystemLoader(DIR_PUBLIC.'views');
	
	$twig = new \Twig\Environment($loaderTemplates);

	$theViewARGS = NULL;
	
	$pages = GetPages::instance();

	$viewARGSaddPages = array_merge($viewARGS, $pages -> get_page_args());

	$args = array (
		'page' => array(
			'view' => $viewARGSaddPages,
			'host' => HOST
		)
	);

	ResCode(200);
	
	if ($view != null)
		echo $twig -> render($view.'.html', $args);
}
