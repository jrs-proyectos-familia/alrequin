<?php
namespace config\php\route;

function getHeaders () {
	$HEADERS = array();

	// Esto es debido a que Fetch convierte todas las cabeceras "Headers" en lowercase
	foreach (getallheaders() as $header => $value)
		$HEADERS[strtolower($header)] = $value;

	return $HEADERS;
}

class RouteTools {
	/**
	 * Método que limpia la URL de alguna injección js o php
	 *
	 * @param [String] $url
	 * @return String La url limpia
	 */
	protected static function clearURL($url) {
		$forbidden = array(
			'*','<script>', '</script>', '<script type=', 'script', '?', '=', '<?php', '?>', '<', '>',
        '.', ',', '^', '´', '+'
		);

		return str_replace($forbidden, '', strtolower($url));
	}

	/**
	 * Método que analiza cualquier URL proveniente del navegador
	 *
	 * @param [String] $url La url sin analizar
	 * @return String La Url analizada
	 */
	protected static function parseURL($url) {
		$newURL = null;

		if (isset($url) && !empty($url)) {
			$newURL = self::clearURL($url);
			$newURL = trim($url, '/');
			$newURL = filter_var($newURL, FILTER_SANITIZE_URL);
			$newURL = explode('/', $newURL);
		}
		return $newURL;
	}

	protected static function validRoute ($route) {
		if (!is_null($route))
			if (!empty($route))
				if (!preg_match("/[áÁéÉóÓúÚñ\\\!$%&()=|<>?¡¿,.@#'*+-]/", $route)) return true;
		
				return false;
	}
}
