<?php
namespace config\php\exceptions;

use config\php\exceptions\Actions;
use config\php\lang\Lang;

use app\server\php\tools\Text;

class DataManageException extends \Exception {

	protected $code = NULL;

	private $isError = false;
	private $langContent = array(
		'EXCE' => NULL,
		'DME' => NULL
	);

	/**
	 * Clase encargada de generar un error para el manejador de datos
	 *
	 * @param string $code Código del error
	 * @param string $nameReference Referencía en donde se produjo el error
	 * @param string $shortMessage Mensaje corto del error
	 */
	public function __construct($code, string $nameReference, string $shortMessage = NULL) {
		$this -> langContent['EXCE'] = Lang::getLang('exce_exceptions', true);
		$this -> langContent['DME'] = Lang::getLang('exce_data_manage', true);

		if (gettype($code) === 'string') {
			$this -> code = $code;

			$message = Lang::getTextLang($this -> code, $this -> langContent['DME']);

			if (is_null($message)) {
				if (!empty($shortMessage)) $message = Text::cutString($shortMessage, 30);
				else {
					$this -> code = 'EXCE-001';
					$this -> isError = true;
				}
			} else {
				$message = Text::replaceFirst("#####", $nameReference, $message);
			}

			parent::__construct($message);
		} else {
			$this -> code = 'EXCE-000';
			$this -> isError = true;
		}

		if ($this -> isError) {
			$this -> message = Lang::getTextLang($this -> code, $this -> langContent['EXCE']);
			throw $this;
		}
	}

	/**
	 * Método especial para imprimir el error en la web
	 *
	 * @return string Mensaje del error
	 */
	public function __toString () {
		$exceParts = explode('\\', __CLASS__);
		
		return Actions::lauchException(array_pop($exceParts), $this);
	}
}
