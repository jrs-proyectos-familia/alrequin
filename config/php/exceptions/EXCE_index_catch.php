<?php
namespace config\php\exceptions;

use const config\php\args\MODE;
use config\php\exceptions\Actions;
use config\php\route\ResCode;

class ErrorsCatch {

	private static $instance = NULL;

	public static function init () {
		if (is_null(self::$instance))
			self::$instance = new ErrorsCatch();

		return self::$instance;
	}
	
	private $errorCatch = array(
		'title' => NULL,
		'exception' => NULL
	);
	
	private function __construct () {
		$this -> errorCatch['title'] = NULL;
		$this -> errorCatch['exception'] = NULL;
	}


	/**
	 * Método que encapsula el contenido de una exception para su próxima impresion
	 *
	 * @param \Exception $e Exception generada durante la aplicación
	 * @return void
	 */
	public function catchException (\Exception $e) {
		switch (get_class($e)) {
			case "PDOException":
			case "Exception":
				$this -> errorCatch['title'] = get_class($e);
				$this -> errorCatch['exception'] = $e;			
				break;
			default:
				$exceParts = explode('\\', get_class($e));
				$exceName = array_pop($exceParts);		
		
				$this -> errorCatch['title'] = $exceName;
				$this -> errorCatch['exception'] = $e;			
				break;
		}
	}

	private function printErrorCatch () {
		if (MODE === 'dev') {
			print(Actions::lauchException(
				$this -> errorCatch['title'],
				$this -> errorCatch['exception']
			));
		} else {
			error_log(
				"[CODE]: {$this -> errorCatch['exception'] -> getCode()} ".
				"[LINE]: {$this -> errorCatch['exception'] -> getLine()} ".
				"[FILE]: {$this -> errorCatch['exception'] -> getFile()} ".
				"[MESSAGE]: {$this -> errorCatch['exception'] -> getMessage()}"
				, 0);
		}
	}

	public function finallException () {
		$this -> printErrorCatch();
		
		ResCode::init() -> response(500, ["error" => "An error has occurred in the system"]);
	}

}
