<?php
namespace config\php\exceptions;

use config\php\exceptions\Actions;
use config\php\lang\Lang;

use app\server\php\tools\Text;

class AnnReaderException extends \Exception {

	protected $code = NULL;

	private $isError = false;
	private $langContent = array(
		"EXCE" => NULL,
		"ANN_RDE" => NULL
	);

	/**
	 * Constructor del método el cual generará una exception de tipo annotation reader
	 *
	 * @param string $code Código especifico del error lanzado
	 * @param string $shortMessage Mensaje corto por si no se logra encontrar el mensaje del error en el apartado de idiomas
	 */
	public function __construct ($code, string $shortMessage = NULL) {
		$this -> langContent['EXCE'] = Lang::getLang('exce_exceptions', true);
		$this -> langContent['ANN_RDE'] = Lang::getLang('exce_ann_reader', true);
		
		if (gettype($code) === 'string') {
			$this -> code = $code;

			$message = Lang::getTextLang($this -> code, $this -> langContent['ANN_RDE']);

			if (is_null($message)) {
				if (!empty($shortMessage)) $message = Text::cutString($shortMessage, 30);
				else {
					$this -> code = "EXCE-001";
					$this -> isError = true;
				}
			} else {
				if (!empty($shortMessage)) {
					$message .= "<br><br>";
					$message .= $shortMessage;
				}
			}

			parent::__construct($message);
		} else {
			$this -> code = "EXCE-000";
			$this -> isError = true;
		}

		if ($this -> isError) {
			$this -> message = Lang::getTextLang($this -> code, $this -> langContent['EXCE']);
			throw $this;
		}
	}

	/**
	 * Método para imprimir el error en la web
	 *
	 * @return string Mensaje del error
	 */
	public function __toString () {
		$exceParts = explode("\\", __CLASS__);

		return Actions::lauchException(array_pop($exceParts), $this);
	}

}
