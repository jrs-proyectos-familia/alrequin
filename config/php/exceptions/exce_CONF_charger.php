<?php
namespace config\php\exceptions;

use config\php\exceptions\Actions;
use config\php\lang\Lang;

use app\server\php\tools\Text;

class ChargerException extends \Exception {

	protected $code = NULL;

	private $isError = false;
	private $langContent = array(
		'EXCE' => NULL,
		'CHE' => NULL
	);

	/**
	 * Clase encargada de generar un error para el cargador de clases y funciones
	 *
	 * @param string $code Código del error
	 * @param string $fileName Nombre del archivo que causo el error
	 * @param string $shortMessage Mensaje corto del error
	 */
	public function __construct($code, string $fileName, string $shortMessage = NULL) {
		$this -> langContent['EXCE'] = Lang::getLang('exce_exceptions', true);
		$this -> langContent['CHE'] = Lang::getLang('exce_charger', true);

		if (gettype($code) === 'string') {
			$this -> code = $code;

			$message = Lang::getTextLang($this -> code, $this -> langContent['CHE']);

			if (is_null($message)) {
				if (!empty($shortMessage)) $message = Text::cutString($shortMessage, 30);
				else {
					$this -> code = 'EXCE-001';
					$this -> isError = true;
				}
			} else {
				$message = Text::replaceFirst('#####', '<b>'.$fileName.'</b>', $message);
			}

			parent::__construct($message);
		} else {
			$this -> code = 'EXCE-000';
			$this -> isError = true;
		}

		if ($this -> isError) {
			$this -> message = Lang::getTextLang($this -> code, $this -> langContent['EXCE']);
			throw $this;
		}
	}

	/**
	 * Método que genera la impresión del error para la web
	 *
	 * @return string Mensaje del error
	 */
	public function __toString () {
		$exceParts = explode('\\', __CLASS__);

		return Actions::lauchException(array_pop($exceParts), $this);
	}

}
