<?php
namespace config\php\exceptions;

use config\php\templates\Exceptions;

class Actions {

	/**
	 * Método que construye el error en su forma visual (Basado en un template HTML) para mostrarse al desarrollador en pantalla
	 *
	 * @param string $errorTitle Título del error
	 * @param \Exception $e Objeto del error a renderizar
	 * @return string Template Html para mostrar el error
	 */
	public static function lauchException (string $errorTitle, \Exception $e) {
		return Exceptions::errorException($errorTitle, $e -> getCode(), $e -> getFile(), $e -> getLine(), $e -> getMessage());
	}

}
