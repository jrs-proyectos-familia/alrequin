<?php
namespace config\php\exceptions;

use config\php\exceptions\FatalErrorException;
use config\php\exceptions\Actions;

function errorHandler ($code, $message, $file, $line) {
	$fatalError =  new FatalErrorException($code, $message, $file, $line);

	print(Actions::lauchException("FATAL ERROR", $fatalError));
}

function fatalError () {
	$lastError = error_get_last();

	if ($lastError['type'] === E_ERROR || $lastError['type'] === 4) {
		// Aqui atrapamos los Fatal Erros
		errorHandler(E_ERROR, $lastError['message'], $lastError['file'], $lastError['line']);
	}
}

set_error_handler('config\php\exceptions\errorHandler');
register_shutdown_function('config\php\exceptions\fatalError');
