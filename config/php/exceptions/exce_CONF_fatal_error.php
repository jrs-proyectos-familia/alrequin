<?php
namespace config\php\exceptions;

use config\php\exceptions\Actions;

class FatalErrorException extends \Exception {
	
	public function __construct (int $code, string $message, string $file, int $line) {
		$this -> code = $code;
		$this -> message = $message;
		$this -> file = $file;
		$this -> line = $line;
	}

	public function __toString() {
		$exceParts = explode("\\", __CLASS__);

		return Actions::lauchException(array_pop($exceParts), $this);
	}
}
