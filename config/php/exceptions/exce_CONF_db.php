<?php
namespace config\php\exceptions;

use config\php\exceptions\Actions;
use config\php\lang\Lang;

use app\server\php\tools\Text;

class DbException extends \Exception {
	
	protected $code = NULL;

	private $isError = false;
	private $langContent = array(
		"EXCE" => NULL,
		"DBE" => NULL
	);

	public function __construct ($code, string $shortMessage = NULL) {
		$this -> langContent['EXCE'] = Lang::getLang('exce_exceptions', true);
		$this -> langContent['DBE'] = Lang::getLang('exce_db', true);

		if (gettype($code) === 'string') {
			$this -> code = $code;

			$message = Lang::getTextLang($this -> code, $this -> langContent['DBE']);

			if (is_null($message)) {
				if (!empty($shortMessage)) $message = Text::cutString($shortMessage, 30);
				else {
					$this -> code = 'EXCE-001';
					$this -> isError = true;
				}
			}

			parent::__construct($message);
		} else {
			$this -> code = 'EXCE-000';
			$this -> isError = true;
		}

		if ($this -> isError) {
			$this -> message = Lang::getTextLang($this -> code, $this -> langContent['EXCE']);
			throw $this;
		}
	}

	public function __toString() {
		$exceParts = explode("\\", __CLASS__);
		return Actions::lauchException(array_pop($exceParts), $this);
	}
}
