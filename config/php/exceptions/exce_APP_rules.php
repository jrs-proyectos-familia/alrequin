<?php
namespace config\php\exceptions;

use config\php\exceptions\Actions;
use config\php\lang\Lang;

use app\server\php\tools\Text;

class RulesException extends \Exception {

	protected $code = NULL;

	private $isError = false;
	private $langContent = array(
		"EXCE" => NULL,
		"RLE" => NULL
	);

	/**
	 * Clase encargada de generar un error para las rules usadas en el proceso mwtools
	 *
	 * @param string $code Código del error
	 * @param string $name_rule Nombre de la regla (método) dónde se produjo el error
	 * @param string $shortMessage Mensaje corto del error
	 */
	public function __construct ($code, string $name_rule, string $shortMessage = NULL) {
		$this -> langContent['EXCE'] = Lang::getLang('exce_exceptions', true);
		$this -> langContent['RLE'] = Lang::getLang('exce_rules', true);

		if (gettype($code) === 'string') {
			$this -> code = $code;

			$message = Lang::getTextLang($this -> code, $this -> langContent['RLE']);

			if (is_null($message)) {
				if (!empty($shortMessage)) $message = Text::cutString($shortMessage, 30);
				else {
					$this -> code = 'EXCE-001';
					$this -> isError = true;
				}
			} else {
				$message = Text::replaceFirst("#####", $name_rule, $message);
			}

			parent::__construct($message);
		} else {
			$this -> code = 'EXCE-000';
			$this -> isError = true;
		}

		if ($this -> isError) {
			$this -> message = Lang::getTextLang($this -> code, $this -> langContent['EXCE']);
			throw $this;
		}
	}

	public function __toString () {
		$exceParts = explode("\\", __CLASS__);

		return Actions::lauchException(array_pop($exceParts), $this);
	}

}
