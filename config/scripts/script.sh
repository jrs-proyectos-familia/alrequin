#!/bin/bash

envFile=config/.env
portServer=3000

# Reiniciamos MODE
if grep -q prod $envFile
then
	modeType='prod';
elif grep -q dev $envFile
then
	modeType='dev';
else 
	modeType='null';
fi
if [ $modeType != 'null' ]
then
	sed -i -e 's/'$modeType'/no_mode/g' $envFile;
fi

# Reiniciamos PORT_FRONTEND
sed -i -e 's/'$portServer'/0000/g' $envFile;

# Reiniciamos BROWSER_BACKEND
if grep -q BROWSER_BACKEND $envFile
then
	modeBrowserSync='BROWSER_BACKEND';
elif grep -q BROWSER_CLIENT $envFile
then
	modeBrowserSync='BROWSER_CLIENT';
else
	modeBrowserSync='null'
fi
if [ $modeBrowserSync != 'null' ]
then
	sed -i -e 's/'$modeBrowserSync'/no_browser_sync/g' $envFile;
fi

# Se prepara las variables dependiendo la instrucción package.json
if [ $1 = 'backend' ];
then

	echo '==================================';
	echo 'TRABAJANDO EN MODO TESTING FORMS';
	echo '==================================';
	
	# Cambiamos el mod a "dev"
	if grep -q no_mode $envFile
	then
		sed -i -e 's/no_mode/dev/g' $envFile;
	fi

	# Asignamos el puerto especifico para correr el servidor de desarrollo
	if grep -q 0000 $envFile
	then
		sed -i -e 's/0000/'$portServer'/g' $envFile;
	fi

	# Especificiamos que tipo de desarrollo queremos hacer para configurar el servidor
	if grep -q no_browser_sync $envFile
	then
		sed -i -e 's/no_browser_sync/BROWSER_BACKEND/g' $envFile;
	fi

elif [ $1 = 'frontend' ];
then
	
	echo '==================================';
	echo 'TRABAJANDO EN MODO TESTING VISTAS';
	echo '==================================';
	
	# Cambiamos el mod a "dev"
	if grep -q no_mode $envFile
	then
		sed -i -e 's/no_mode/dev/g' $envFile;
	fi

	# Asignamos el puerto especifico para correr el servidor de desarrollo
	if grep -q 0000 $envFile
	then
		sed -i -e 's/0000/'$portServer'/g' $envFile;
	fi

	# Especificiamos que tipo de desarrollo queremos hacer para configurar el servidor
	if grep -q no_browser_sync $envFile
	then
		sed -i -e 's/no_browser_sync/BROWSER_CLIENT/g' $envFile;
	fi


elif [ $1 = 'production' ];
then

	echo '=====================================================';
	echo 'EMPAQUETADO FINAL LISTO PARA USAR EN MODO PRODUCCIÓN'
	echo '=====================================================';

	# Cambiamos el mod a "prod"
	if grep -q no_mode $envFile
	then
		sed -i -e 's/no_mode/prod/g' $envFile;
	fi
	
else
	echo 'NO SE ECONTRÓ LA PETICIÓN';
fi

webpack

exit
