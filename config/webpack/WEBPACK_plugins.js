const path = require('path')

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const Dotenv = require('dotenv-webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const { WebpackServer } = require('./webpack.server')

class WebpackPlugins {

	constructor () {
		this.thePlugins = []
	}

	miniCss (env) {
		return new MiniCssExtractPlugin({
				filename: env.MODE === 'prod' ? '../css/[name]/[hash].css' : '../css/[name]/[name].css',
				chunkFilename: '[id].css'
			})
	}

	vueLoader () {
		return new VueLoaderPlugin()
	}

	dotEnv () {
		return new Dotenv()
	}

	buildingHtmlPages (view, env, twigParamsDevFrontend) {
		return new HtmlWebpackPlugin({
			filename: env.MODE === 'prod' || env.BROWSER_SYNC === 'BROWSER_BACKEND' ? `../views/${view}.html` : `../${view}.html`,
			inject: true,
			hash: true,
			cache: false,
			template: env.MODE === 'prod' ? path.resolve(__dirname, '../../app/client/config/twig/indexBack.twig') : path.resolve(__dirname, '../../app/client/config/twig/indexFront.twig'),
			templateParameters: twigParamsDevFrontend,
			minify: {
				collapseWhitespace: env.MODE === 'prod' ? true : false,
				removeComments: env.MODE === 'prod' ? true : false,
				removeRedundantAttributes: env.MODE === 'prod' ? true : false,
				removeScriptTypeAttributes: env.MODE === 'prod' ? true : false,
				removeStyleLinkTypeAttributes: env.MODE === 'prod' ? true : false,
				useShortDoctype: env.MODE === 'prod' ? true : false
			},
			chunks: [view]
		})
	}

	static plugins (env, views, {publicDir, twigParamsDevFrontend}) {
		const wp = new WebpackPlugins()
		
		wp.thePlugins.push(wp.miniCss(env))
		wp.thePlugins.push(wp.vueLoader())
		wp.thePlugins.push(wp.dotEnv())

		for (const view of views)
			wp.thePlugins.push(wp.buildingHtmlPages(view, env, twigParamsDevFrontend))

		const server = WebpackServer.server(env, publicDir)
		if (server !== null) wp.thePlugins.push(server)
		
		return wp.thePlugins
	}
}

module.exports = {WebpackPlugins}
