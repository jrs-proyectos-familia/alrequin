const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

class WebpackOptimization {

	uglifyJsPlugin () {
		return new UglifyJsPlugin({
			uglifyOptions: {
				output: {
					comments: false,
				}
			}
		})
	}

	static optimization () {
		const wo = new WebpackOptimization()

		return {
			minimizer: [
				wo.uglifyJsPlugin()
			] 
		}
	}

}

module.exports = {WebpackOptimization}
