const path = require('path')
const fs = require('fs')

module.exports = function (env) {
	const publicDir = path.resolve(__dirname, '../../app/public')
	const hostDevFrontend = `http://localhost:${env.PORT_FRONTEND}/`
	const twigParamsDevFrontend = {
		'page': {
			'view': {
				'title': 'Arlequin Modo de Desarrollo',
				'pages': {}
			},
			'host': env.HOST
		}
	}

	// Estableciendo parametros para el funcionamiento del modo DEV de webs staticas o dinamicas (Frontend ó Frontend-Backend (Modo Dev Proxy))
	const views = [];

	fs
		.readdirSync(path.resolve(__dirname, '../../app/client/views'))
		.forEach(view => {
			const file = path.join(__dirname, '../../app/client/views', view, `${view}.js`);
			
			if (fs.existsSync(file)) views.push(view)
		})

	views.forEach(view => {
		if (env.BROWSER_SYNC === 'BROWSER_BACKEND') {
			// Para el Modo Frontend con Backend (Server Dev Proxy PHP)

			twigParamsDevFrontend['page']['view']['pages'][view] = {
				route: null,
				name: null,
				alias: null
			}

			const file = path.resolve(__dirname, '../../app/server/php/views', view, '_view.php');

			if (fs.existsSync(file)) {
				fs.createReadStream(file, {
					encoding: 'utf8',
				})
					.on('data', (data) => {
						const lines = data.split('\n')

						let textNameAlias = null
						let textRoute = null

						for (const line of lines) {
							if (line.search('"title"') > 0) textNameAlias = line
							else if (line.search("'title'") > 0) textNameAlias = line

							if (line.search('"mainRoute"') > 0) textRoute = line
							else if (line.search("'mainRoute'") > 0) textRoute = line
						}


						if (textNameAlias !== null) {
							const nameView = textNameAlias.split('=>')[1].replace(/'/g, ' ').replace(/"/g, ' ').trim()
							twigParamsDevFrontend['page']['view']['pages'][view]['router'] = nameView.toLowerCase()
							twigParamsDevFrontend['page']['view']['pages'][view]['alias'] = nameView.toLowerCase()
						}
						if (textRoute !== null) {
							const routeView = textRoute.split('=>')[1].replace(/'/g, ' ').replace(/"/g, ' ').replace(/,/g, ' ').trim()
							twigParamsDevFrontend['page']['view']['pages'][view]['link'] = env.HOST + routeView
						}
						
					})
			}
		} else {
			// Para el Modo Frontend (Webs Staticas)

			twigParamsDevFrontend['page']['view']['pages'][view] = {
				link: env.HOST + view + '.html',
				router: view + '.html',
				alias: view.toLowerCase()
			}

			twigParamsDevFrontend.page.host = `http://localhost:${env.PORT_FRONTEND}/`
		}

	});

	return {
		WebpackArgs: {
			publicDir,
			hostDevFrontend,
			twigParamsDevFrontend
		}
	}
}
