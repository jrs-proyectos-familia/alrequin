const fs = require('fs')
const Rimraf = require('rimraf')

class WebpackTools {
	
	/**
	 * 
	 * @param {STRING} publicDir Directorio en donde se encuentra la carpeta public creada a partir del código del cliente
	 */
	static removePublic (publicDir) {
		if (fs.existsSync(publicDir)) {
			Rimraf(publicDir, err => {
				if (err) {
					console.log('===============================================')
					console.log('Hubo un error al eliminar la carpeta public del cliente ;(')
					console.log(err)
					console.log('===============================================')
					process.exit()
				} else {
					console.log('===============================================')
					console.log('Eliminado la carpeta public cliente :)')
					console.log('===============================================')
				}
			})
		}
	}

}

module.exports = {WebpackTools}
