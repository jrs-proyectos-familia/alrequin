const path = require('path')

class WebpackAlias {

	static resolve () {
		return {
			extensions: ['.js', '.sass', 'scss', '.vue', '.twig'],
			alias: {
				'@Assets': path.resolve(__dirname, '../../app/client/assets'),
				'@Components': path.resolve(__dirname, '../../app/client/components'),
				'@Mixins': path.resolve(__dirname, '../../app/client/mixins'),
				'@Tools': path.resolve(__dirname, '../../app/client/tools'),
				'@Views': path.resolve(__dirname, '../../app/client/views'),
				'@Store': path.resolve(__dirname, '../../app/client/store-vuex')
			}
		}
	}

}

module.exports = {WebpackAlias}
