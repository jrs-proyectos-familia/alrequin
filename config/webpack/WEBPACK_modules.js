const MiniCssExtractPlugin = require('mini-css-extract-plugin')

class WebpackModule {

	moduleJavascript () {
		return {
			test: /\.js$/,
			exclude: [/node_modules/, './bin/webpack.entries.js', './bin/webpack.conf.js','./app/client/js/'],
			use: {
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env'],
					plugins: [
						'@babel/plugin-transform-runtime'
					]
				}
			}
		}
	}

	moduleCss () {
		return {
			test: /\.css$/,
			use: [
				{ loader: MiniCssExtractPlugin.loader },
				{ loader: 'css-loader' },
				{
					loader: 'postcss-loader', options: {
						indent: 'postcss',
						plugins: loader => [
							require('autoprefixer')(),
							require('cssnano')()
						]
					}
				},
			]
		}
	}

	moduleVue () {
		return {
			test: /\.vue/,
			loader: 'vue-loader'
		}
	}

	moduleSass () {
		return {
			test: /\.sass$/,
			use: [
				'vue-style-loader',
				'css-loader',
				{
					loader: 'sass-loader',
					options: {
						indentedSyntax: true
					}
				}
			]
		}
	}

	modulePug () {
		return {
			test: /\.pug$/,
			loader: 'pug-plain-loader'
		}
	}

	moduleImage (env, hostDevFrontend) {
		return {
			test: /\.(jpe?g|png|gif|svg)$/i,
			use: [
				{
					loader: 'file-loader', options: {
						name: env.MODE === 'prod' ? '[hash].[ext]' : '[name].[ext]',
						publicPath: env.MODE === 'prod' || env.BROWSER_SYNC === 'BROWSER_BACKEND' ? env.HOST + 'app/public/img/' : hostDevFrontend + '../img/',
						outputPath: '../img'
					}
				}
			]
		}
	}

	moduleTwig () {
		return {
			test: /\.twig$/,
			loader: 'twig-loader'
		}
	}

	static module (env, {hostDevFrontend}) {
		const wm = new WebpackModule()
	
		return  {
			rules: [
				wm.moduleJavascript(),
				wm.moduleCss(),
				wm.moduleSass(),
				wm.moduleImage(env, hostDevFrontend),
				wm.modulePug(),
				wm.moduleVue(),
				wm.moduleTwig()
			]
		}
	}

}

module.exports = {WebpackModule}
