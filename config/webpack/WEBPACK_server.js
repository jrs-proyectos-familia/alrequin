const path = require('path')

const BrowserSyncWebpackPlugin = require('browser-sync-webpack-plugin')

class WebpackServer {
	static server (env, publicDir) {
		if (env.MODE === 'dev') {

			const optionsBrowserSync = {
				host: 'localhost',
				port: env.PORT_FRONTEND,
				open: false, // Por si se desea abrir el navegador al terminar de empaquetar el proyecto
				browser: ['min', 'firefox'],
				notify: false, // Ayuda a eliminar las notificaciones en el navegador
			}
		
			// Aqui defino si se usara el servidor en forma de servidor cliente o en forma de proxy para funcionar a la par con el backend PHP
			if (env.BROWSER_SYNC === 'BROWSER_CLIENT') {
				optionsBrowserSync['server'] = {
					baseDir: path.resolve(publicDir)
				}
			} else if (env.BROWSER_SYNC === 'BROWSER_BACKEND')
				optionsBrowserSync['proxy'] = env.HOST
		
			return new BrowserSyncWebpackPlugin(optionsBrowserSync, {
				reload: true
			})
		} else return null
	}
}

module.exports = {WebpackServer}
