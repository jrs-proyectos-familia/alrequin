const path = require('path')
const fs = require('fs')

class WebpackEntries {
	constructor () {
		this.entriesJS = {}
	}

	static get entriesJS () {
		const we = new WebpackEntries()

		fs
			.readdirSync(path.resolve(__dirname, '../../app/client/views'))
			.forEach(view => {
				const pathJS = path.join(__dirname, '../../app/client/views', view, `${view}.js`)

				if (fs.existsSync(pathJS))
					we.entriesJS[view] = pathJS
			})

		return we.entriesJS
	}

	static output (env, {publicDir, hostDevFrontend}) {
		return {
			filename: env.MODE === 'prod' ? '[name]/[hash].js' : '[name]/[name].js',
			path: path.resolve(publicDir, 'js'),
			publicPath: env.MODE === 'prod' || env.BROWSER_SYNC === 'BROWSER_BACKEND' ? env.HOST + 'app/public/js/' : hostDevFrontend + 'js/'
		}
	}
}

module.exports = {WebpackEntries}
