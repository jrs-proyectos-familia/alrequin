<?php
namespace doctrine\config;

use Doctrine\Common\Annotations\AnnotationReader;

class Lector {

	public static function anotacion (object $prop, $anotacion) {
		$reader = new AnnotationReader();
		$anotacion = $reader -> getClassAnnotations($prop, $anotacion);

		return $anotacion;
	}

	public static function anotacion2 ($prop, $anotacion) {
		$reader = new AnnotationReader();
		$anotacion = $reader->getPropertyAnnotation(
				$prop,
				$anotacion
		);

		return $anotacion;
	}
}

