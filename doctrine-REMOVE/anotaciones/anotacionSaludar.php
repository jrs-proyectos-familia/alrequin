<?php
namespace doctrine\b;

/**
 * @Annotation
 * @Target("CLASS")
 */
class Anotacion {

	/**
	 * Mensaje del saludo
	 *
	 * @Enum({"HOLA", "BUENOS DIAS"})
	 */
	public $mensaje;
	public $autor;
	private $fecha = "JUEVES";
	public $aviso;

	public function getFecha () {
		return $this -> fecha;
	}

}
