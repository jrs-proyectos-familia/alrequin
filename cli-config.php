<?php
// Configuración del CLI de Doctrine para poder usarlo la terminal

require_once('config'.DIRECTORY_SEPARATOR.'php'.DIRECTORY_SEPARATOR.'args'.DIRECTORY_SEPARATOR.'CONF_args.php');
require_once('config'.DIRECTORY_SEPARATOR.'php'.DIRECTORY_SEPARATOR.'db'.DIRECTORY_SEPARATOR.'db_CONF_doctrine.php');

use config\php\db\DoctrineORM;

$entityManager = DoctrineORM::init() -> getEntityManager();

$conn = $entityManager -> getConnection();

$conn -> getDatabasePlatform() -> registerDoctrineTypeMapping('enum', 'string');

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
