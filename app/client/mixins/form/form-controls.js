import { ButtonSend } from '@Tools/form/buttonSend'

export default {
	data () {
		return {
			disabledSend: true,
			serverError: '',
			dialog: {
				status:false,
				message: ''
			}
		}
	},

	watch: {
		form: {
			handler (value, oldValue) {
				let activeSend = null

				if (typeof this.oldReg === 'undefined')
					activeSend = ButtonSend.activeSend(value)
				else activeSend = ButtonSend.activeSendUpdate(this.oldReg, value)

				if (activeSend) this.disabledSend = false
				else this.disabledSend = true
			},
			deep: true
		}
	},

	methods: {
		clear (value) {
			if (value) {
				for (const field of Object.keys(this.form)) {
					if (this.form[field].type === 'inputOneFile') {
						this.form[field].value = null
						this.$eventBus.$emit(`[Global-FormInputFile]:reset_${this.form[field].label}`)
					}
					else if (this.form[field].type === 'inputText')
						this.form[field].value = ''
				}
	
				this.clearErrors()
			}
		},
	
		clearErrors () {
			this.serverError = ''
			
			for (const field of Object.keys(this.form))
				this.form[field].error = null
		},
	
		showErrors (errors) {
			for (const field of Object.keys(errors))
				this.form[field].error = errors[field]
		},
	
		closeDialog (value) {
			if (value)
				this.dialog.status = false
		}
	}

}
