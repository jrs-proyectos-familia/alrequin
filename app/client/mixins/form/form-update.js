export default {
	methods: {
		updateOldReg () {
			for (const field of Object.keys(this.form)) {
				this.form[field].value = this.getUser[field]
				this.form[field].hint = this.getUser[field]

				this.oldReg[field].value = this.getUser[field]
			}
		}
	}
}
