const mixin = {
	created () {
		this.links = $PAGE.view.pages;
	},

	data () {
		return {
			links: null
		}
	},

	methods : {
		logo () {
			window.location.href=`${$PAGE.host}`
		}
	}
}

export default mixin;
