import '@Assets/styles/css/index.css'

import Vue from 'vue'
import store from '@Store/storeVuex'

import User from './user.vue'
import Vuetify from 'vuetify'
import VuetifyOptions from '@Assets/js/vuetify'
import router from './router'

Vue.use(Vuetify, VuetifyOptions)

Vue.prototype.$eventBus = new Vue()

new Vue ({
	el: '#app',
	router,
	store,
	render: h => h(User)
})
