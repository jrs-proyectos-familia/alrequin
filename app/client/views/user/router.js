import Vue from 'vue'
import VueRouter from 'vue-router'

import NewUser from './ghost-pages/gp-newUser'
import UpdateUser from './ghost-pages/gp-updateUser'
import UpdateUserName from './ghost-pages/gp-updateUserName'
import UpdateUserAlias from './ghost-pages/gp-updateUserAlias'
import UpdateUserImage from './ghost-pages/gp-updateUserImage'

import { FixRoute } from '@Tools/router/fixRoute'
import { AuthPage } from '@Tools/router/authPage'

Vue.use(VueRouter)

// El vue-router esta pensado para crear páginas SPA "FANTASMAS" de la vista principal (La cual ha sido cargado por el backend). Estas páginas "fantasma" no tendrán control directo del backend; es decir, no las renderizará el backend, y como tal solo sirven como medio para desplegar diversa información sin la necesidad de sobresaturar el proceso del backend a la hora de renderizar páginas de poco flujo de datos con el servidor, son ideales para paginas simples.
// Importante.- Como alternativa, estas páginas pueden requerir datos al backend por medio de sockets
const router = new VueRouter({
	mode: 'history', // IMPORTANTE: Si se va a usar esta propiedad es recomendable añadir en el path 1er Hijo, la vista en la cual se esta renderizando (Sería como la parte index), esto solo aplica a las rutas primarias, las CHILDREN routes no aplica
	routes: [
		{
			path: FixRoute.getMainRoute($PAGE.view.pages.user.router),
			name: 'NewUser',
			component: NewUser
		},
		{
			path: `${FixRoute.getMainRoute($PAGE.view.pages.user.router)}/:alias`,
			name: 'UpdateUser',
			component: UpdateUser,
			meta: {
				auth: true
			},
			children: [
				{
					path: 'editar/nombre',
					name: 'UpdateUserName',
					component: UpdateUserName,
					meta: {
						auth: true
					}
				},
				{
					path: 'editar/alias',
					name: 'UpdateUserAlias',
					component: UpdateUserAlias,
					meta: {
						auth: true
					}
				},
				{
					path: 'editar/imagen',
					name: 'UpdateUserImage',
					component: UpdateUserImage,
					meta: {
						auth: true
					}
				}
			]
		},
		{
			path: '*',
			redirect: to => {
				window.location = $PAGE.view.pages.user.link
			}
		}
	]
})

AuthPage.checkPage(router, $PAGE.view.pages.user.router)

export default router;
