import '@Assets/styles/css/index.css'

import Vue from 'vue';
import store from '@Store/storeVuex'

import Home from './home.vue'
import Vuetify from 'vuetify'
import VuetifyOptions from '@Assets/js/vuetify'

Vue.use(Vuetify, VuetifyOptions)

Vue.prototype.$eventBus = new Vue();

new Vue ({
	el: '#app',
	store,
	render: h => h(Home)
})
