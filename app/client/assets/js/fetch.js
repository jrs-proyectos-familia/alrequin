import store from '@Store/storeVuex'

function theHeaders (type) {
	const headers = new Headers()
	
	switch (type) {
		case 'basic-json':
			headers.append('Content-Type', 'application/json')
			headers.append('Accept', 'application/json')
			if (store.state.login.token !== null)
				headers.append('Authorization', `Token ${store.state.login.token}`)
			break
		case 'basic-multipart':
			headers.append('Accept', 'multipart/form-data')
			if (store.state.login.token !== null)
				headers.append('Authorization', `Token ${store.state.login.token}`)
			break
		default:
			console.error('No se pudo armar los headers necesarios para enviar la petición')
	}
	
	return headers
}

export const fetchConfig = {
	/**
	 * 
	 * @param {JSON} data Datos a ser enviados al backend
	 * @param {STRING} url Url a la cual serán enviados los datos
	 * @param {STRING} method Método REST que se usará para enviar los datos
	 * @param {STRING} typeHeaders Tipo de Headers que se enviarán
	 */
	getRequest (data, url, method, typeHeaders = 'basic-json') {
			const options = {
				method: method,
				cache: 'no-store',
				headers: theHeaders(typeHeaders)
			}
			if (data !== null) {
				if (typeHeaders === 'basic-json')
					options['body'] = JSON.stringify(data)
				else if (typeHeaders === 'basic-multipart')
					options['body'] = data
			}
		
			return new Request(url, options)
	}
}
