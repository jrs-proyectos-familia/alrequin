class Validator {
  static empty (data) {
    let res = null

    if (data === null) res = "Este campo no puede estar vacio"

    return res
  }

  static type (option, data) {
    let res = null

    switch (option) {
      case 'string':
        if (typeof data !== 'string' || !isNaN(data)) res = "Debe de contener solo letras y ningún número"
        break
      case 'number':
        if (isNaN(data)) res = "Debe de contener solo números y ninguna letra"
        break
      case 'email':
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (!re.test(data)) res = "Debe de ser un email valido"
        break
      case 'password':
        if (data.search(/[!@#$%^&*_+].*[!@#$%^&*_+].*[!@#$%^&*_+]/) < 0) res = "Debe de contener al menos 3 carácteres especiales"
        if (data.search(/[a-z].*[a-z].*[a-z]/i) < 0) res = 'Debe de contener al menos 3 letras'
        if (data.search(/[0-9].*[0-9].*[0-9]/) < 0) res = 'Debe de contener al menos 3 números'
        break
      default:
        res = "Se ha encontrado un error al intentar validar el tipo de dato"
    }

    return res
  }

  static min (data, min) {
    let res = null

    if (data.length < min) res = `Debe de contener como mínimo ${min} carácteres. Te faltan ${min - data.length} carácteres`

    return res
  }

  static max (data, max) {
    let res = null

    if (data.length > max) res = `Debe de contener como máximo ${max} carácteres. Te sobran ${data.length - max} carácteres`

    return res
  }

  static equal (data, compare) {
    let res = null

    if (data !== compare.data) res = `Debe de coincidir con el valor ingresado del campo ${compare.message}`

    return res
  }

  static noEqual (data, compare) {
    let res = null

    if (data === compare.data) res = `No debe de coincidir con el valor ingresado del campo ${compare.message}`

    return res
  }

  static extensions (option, data, listExtensions) {
    let res = null

    switch (option) {
      case 'image':
        if (!listExtensions.includes(data)) res =  `Debe de ser un archivo de tipo imagen... se aceptan los siguientes formatos ${listExtensions.toString()}`
        break
      default:
        res = "Se ha encontrado un error al intentar validar el tipo de archivo"
    }

    return res
  }
}

class ValidatorMethods {
  static basicMain (data, type, min, max) {
    let res = null

    res = Validator.empty(data)
    if (res === null) {
      res = Validator.type(type, data)
      if (res === null) {
        res = Validator.min(data, min)
        if (res === null) {
          res = Validator.max(data, max)
          if (res === null) return null
        }
      }
    }

    return res
  }

  static basicMainOptionalEmpty (data, type, min, max) {
    let res = null

    if (Validator.empty(data) === null) {
      res = Validator.type(type, data)
      if (res === null) {
        res = Validator.min(data, min)
        if (res === null) {
          res = Validator.max(data, max)
          if (res === null) return null
        }
      }
    }

    return res
  }
}

export { Validator, ValidatorMethods }
