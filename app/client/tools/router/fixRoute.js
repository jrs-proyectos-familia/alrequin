class FixRoute {
	static getMainRoute (view) {
		let mainRoute = '/'

		let routeCleanArray = null;
		
		const routeArray = $PAGE.host.split('/')
		
		let regexMain = new RegExp(/^http/)

		if (regexMain.test($PAGE.host)) {
			routeCleanArray = routeArray.filter(part => {
				let regex = RegExp('^http')
				
				if (part.length)
					if (!regex.test(part)) return part
			})
		} else {

			regexMain = new RegExp(/^https/)

			if (regexMain.test($PAGE.host)) {
				routeCleanArray = routeArray.filter(part => {
					let regex = RegExp('^https')
					
					if (part.length)
						if (!regex.test(part)) return part
				})
			}

		}
	
		if (routeCleanArray !== null) {
			routeCleanArray.shift()

			if (routeCleanArray.length)
				mainRoute = `/${routeCleanArray.join('/')}/${view}`
			else mainRoute = `/${view}`
		}
		
		return mainRoute
	}
}

export { FixRoute }
