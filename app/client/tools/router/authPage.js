import { fetchConfig } from '@Assets/js/fetch'

class AuthPage {
	/**
	 * 
	 * @param {JSON} router Configuración global del router 
	 * @param {String} view Página principal de la cual esta arrancando el router
	 */
	static checkPage (router, view) {
		router.beforeEach(async (to, from , next) => {
			let auth = null
			
			const metaParams = Object.keys(to.meta)
			if (metaParams.includes('auth')) {
				auth = to.meta.auth;
				
				if (auth) {
					const request = fetchConfig.getRequest(null, $PAGE.host + 'login/check', 'post', 'basic-json')
					const response = await fetch(request);
			
					if (response.status === 200) next()
					else window.location = $PAGE.host+view // TODO: La idea sería desplegar el modal del login, para que se autentifique facilmente
				} else next() // Cuando Auth = False por ende no se requiere una autorizacion de usuario
			} else next()
			
		})
	}
}

export { AuthPage }
