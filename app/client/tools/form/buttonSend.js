import { Validator, ValidatorMethods } from '@Assets/js/validator'
import { fetchConfig } from '@Assets/js/fetch'

/**
 * Función que retorna los mismos campos del formulario pero ya tratados
 * 
 * @param {JSON} fields  Objeto que contiene la data raw del formulario
 */
function treatmentFields (fields) {
	const treatFields = {}

	for (const field of Object.keys(fields)) {
		treatFields[field] = {
			rule: fields[field].rule
		}
		
		if (fields[field].value !== undefined) {
			if (fields[field].value !== null)
				treatFields[field]['value'] = fields[field].value
			else treatFields[field]['value'] = null
		} else treatFields[field]['value'] = null
	}
	
	return treatFields
}

class ButtonSend {
	/**
	 * Función que establece si el botón de enviar se encuentra listo para mostrarse o no
	 * 
	 * @param  {JSON} fields Objeto que contiene la data raw del formulario
	 */
	static activeSend (fields) {
		let expectedValidate = 0
		let dataValidate = 0

		const treatFields = treatmentFields(fields)
		expectedValidate = Object.keys(treatFields).length

		for (const field of Object.keys(treatFields)) {
			if (treatFields[field].rule.Val_empty){
				if (treatFields[field].value !== undefined) { // Esto es debido a la acción del botón limpiar del formulario (añade undefined a cada value de cada campo)
					// console.log(treatFields[field].value)
					if (treatFields[field].value !== null) dataValidate++
				}
			} else dataValidate++
		}
		
		if (expectedValidate === dataValidate) return true
		return false
	}

	static activeSendUpdate (oldFields, newFields) {
		let activeSend = false
		let dataSame = 0

		if (ButtonSend.activeSend(newFields)) {
			for (const oldField of Object.keys(oldFields))
				if (oldFields[oldField].value === newFields[oldField].value) dataSame++
		}

		if (dataSame !== Object.keys(newFields).length) activeSend = true
		
		return activeSend
	}

	static validate (fields) {
		let errorFields = {}

		const treatFields = treatmentFields(fields)

		for (const field of Object.keys(treatFields)) {
			errorFields[field] = null

			if (treatFields[field].rule.Val_empty)
				errorFields[field] = ValidatorMethods.basicMain(
					treatFields[field].value,
					treatFields[field].rule.Val_text,
					treatFields[field].rule.Val_min,
					treatFields[field].rule.Val_max
				)
			else {
				let value = null;

				switch (fields[field].type) {
					case 'inputOneFile':
						if (treatFields[field].value !== null) {
							if (typeof treatFields[field].value.path !== 'undefined')
								value = treatFields[field].value.path
							else value = treatFields[field].value.name
						}
						break;
					default:
						value = treatFields[field].value
				}

				errorFields[field] = ValidatorMethods.basicMainOptionalEmpty(
					value,
					treatFields[field].rule.Val_text,
					treatFields[field].rule.Val_min,
					treatFields[field].rule.Val_max
				)
			}

		}

		return errorFields
	}

	/**
	 * 
	 * @param {Object} data Objeto que contiene los datos preparados para ser enviados al backend 
	 * @param {string} url Url a la cual serán enviados los datos
	 * @param {method} method Metodo que se usará
	 */
	static async sendFetchFormData (data, url, method) {
		const request = fetchConfig.getRequest(data, $PAGE.host + url, method, 'basic-multipart')

		const response = await fetch(request)
		
		if (response.status === 201 || response.status === 406 || response.status === 500) {
			const dataResponse = await response.json()
			return {res: response, data: dataResponse}
		} else return {res: response}
	}

	/**
	 * 
	 * @param {Object} data Objeto que contiene los datos preparados para ser enviados al backend 
	 * @param {string} url Url a la cual serán enviados los datos
	 * @param {method} method Metodo que se usará
	 */
	static async sendFetchData (data, url, method) {
		const request = fetchConfig.getRequest(data, $PAGE.host + url, method, 'basic-json')
		
		const response = await fetch(request)
		
		if (response.status === 201 || response.status === 406 || response.status === 500) {
			const dataResponse = await response.json()
			return {res: response, data: dataResponse}
		} else return {res: response}
	}
	
}

export { ButtonSend }
