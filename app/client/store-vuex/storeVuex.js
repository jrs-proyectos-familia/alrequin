import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	strict: true,
	plugins:[],
	state: {
		login: {
			user: {
				name: '',
				surname: '',
				second_surname: '',
				alias: '',
				image: null
			},
			token: null
		}
	},
	getters: {
		getUser (state) {
			if (state.login.user.name.length) {
				return {
					name: state.login.user.name,
					surname: state.login.user.surname.length ? state.login.user.surname : null,
					second_surname: state.login.user.second_surname.length ? state.login.user.second_surname : null,
					alias: state.login.user.alias.length ? state.login.user.alias : null,
					image: state.login.user.image !== null ? state.login.user.image : null
				}
			} else {
				return {
					name: null,
					surname: null,
					second_surname: null,
					alias: null,
					image: null
				}
			}
		},

		getToken (state) {
			if (state.login.token !== null) return state.login.token
			return null
		}
	},

	mutations: {
		setUser (state, user) {
			state.login.user.name = user.name
			state.login.user.surname = user.surname
			state.login.user.second_surname = user.second_surname
			state.login.user.alias = user.alias
			state.login.user.image = user.image
		},

		setUserImage (state, image) {
			state.login.user.image = image
		},

		setToken (state, token) {
			state.login.token = token
		},

		destroyUser (state) {
			state.login.user.name = ''
			state.login.user.surname = ''
			state.login.user.second_surname = ''
			state.login.user.alias = ''
			state.login.user.image = null
		},

		destroyToken (state) {
			state.login.token = null
		}
	},

	actions: {
		
	}

})
