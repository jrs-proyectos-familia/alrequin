<?php
namespace app\server\php\mixins;

use const config\php\args\USE_ORM;
use config\php\db\DoctrineORM;

use app\server\php\tools\Text;
use app\server\php\ini\mwtools\{ValRules, Validate, ValidateMethods};

class Val {

	private $valData = NULL;
	private $valRules = NULL;
	private $valMethod = NULL;
	private $valView = NULL;

	public $valErrorsFields = array(); // Errores de los campos del formulario
	public $valErrorMain = NULL; // Error principal que abarca a todos los campos del formulario al a vez

	public function __construct (array $dataTreat, array $valRules, string $method, string $view) {
		if (!count($dataTreat) > 0) throw new \Exception("<b>No se logró cargar los datos a validar</b>");
		
		$firstRule = array_key_first($valRules);
		
		if (!$valRules[$firstRule] instanceof ValRules) throw new \Exception("<b>No se logró cargar las reglas de validación</b>");

		if (empty($method)) throw new \Exception("<b>No se logró cargar el método de la operación</b>");

		if (empty($view)) throw new \Exception("<b>No se logró cargar la vista de la operación</b>");
		
		$this -> valData = $dataTreat;
		$this -> valRules = $valRules;
		$this -> valMethod = $method;
		$this -> valView = $view;
	}

	private function val_basic () {
		$nextVal = true;

		foreach ($this -> valData as $key => $data) {
			$error = NULL;
			
			$isEmpty = $this -> valRules[$key] -> get_empty();

			if ($isEmpty) {
				$error = ValidateMethods::valEmptyTypeMinMax(
					$data,
					$this -> valRules[$key] -> get_type(),
					$this -> valRules[$key] -> get_min(),
					$this -> valRules[$key] -> get_max()
				);
			} else {
				$error = ValidateMethods::valOptionalEmptyTypeMinMax(
					$data,
					$this -> valRules[$key] -> get_type(),
					$this -> valRules[$key] -> get_min(),
					$this -> valRules[$key] -> get_max()
				);
			}

			$valBigger = $this -> valRules[$key] -> get_bigger();

			if ($valBigger !== -1)
				$error = Validate::bigger($data, $valBigger);

			$valLower = $this -> valRules[$key] -> get_lower();
			if ($valLower !== -1)
				$error = Validate::lower($data, $valLower);

			if (!is_null($error)) {
				if ($nextVal) $nextVal = false; // Para que no se repita una vez que sea false será por siempre

				$this -> valErrorsFields[$key] = $error;
			}
		}

		return $nextVal;
	}

	/**
	 * Función que realizará la validación de las imagenes
	 *
	 */
	private function val_image () {
		$nextVal = true;

		foreach ($this -> valData as $key => $data) {
			$error = NULL;
			$ruleImage = $this -> valRules[$key] -> get_image();

			if (count($ruleImage) === 3) {
				$error = ValidateMethods::valImageFile($ruleImage['fileError'], $ruleImage['fileType'], $ruleImage['fileSize']);

				if (!is_null($error)) {
					if ($nextVal) $nextVal = false;

					$this -> valErrorsFields[$key] = $error;
				}
			}
		}

		return $nextVal;
	}

	/**
	 * Función que realizará la validación de servidor "Unica" para cada campo de regla
	 * 
	 */
	private function val_db_unique () {
		$nextVal = true;

		foreach ($this -> valData as $key => $data) {
			$error = NULL;
			$ruleUnique = $this -> valRules[$key] -> get_unique();
			$sql = NULL;
			$dataSQL = array(
				'data' => NULL,
				'id' => NULL
			);
			$error = NULL;

			$field = NULL;
			
			if (count($ruleUnique) === 2)
				$field = $key;
			else if (count($ruleUnique) === 3)
				$field = $ruleUnique['field'];
			
			if (!is_null($field)) {
				switch ($this -> valMethod) {
					case 'post':
						if (empty($data)) {
							if ($ruleUnique['oneNull'] && is_null($data)) {
								// Solo podrá existir un registro NULL por tabla BD
								$sql = "SELECT id FROM {$this -> valView} WHERE $field IS NULL"; 
							} else if (!is_null($data)) {
								$sql = "SELECT id FROM {$this -> valView} WHERE $field = :data";
								$dataSQL['data'] = $data;
							};
						} else {
							// Solo podrá existir un registro $data por tabla BD pero si podrá existir varios registros NULL (Si lo permite la configuración de la BD)
							$dataSQL['data'] = $data;
							if ($ruleUnique['caseSensitive']) 
								$sql = "SELECT id FROM {$this -> valView} WHERE $field = _utf8 :data COLLATE utf8_bin";
							else
								$sql = "SELECT id FROM {$this -> valView} WHERE $field = :data";	
						}
						
						// Si oneNull es false y el $data es NULL, entonces esta validación no se tomará en cuenta y terminará como satisfactoria
						
						if (!is_null($sql))
							$error = Validate::db_unique($sql, $dataSQL);
						break;
					case 'put':
						if (array_key_exists('id', $this -> valData)) {
							// Si no se consigue el id, esta validación siempre será satisfactoría debido a que devolverá el registro a modificar, cosa que no se desea.
							$id = $this -> valData['id'];
							$dataSQL['id'] = $id;

							if (empty($data)) {
								if ($ruleUnique['oneNull'] && is_null($data)) {
									// Solo podrá existir un registro NULL por tabla BD
									$sql = "SELECT id FROM {$this -> valView} WHERE $field IS NULL AND id <> :id"; 
								} else if (!is_null($data)) {
									$sql = "SELECT id FROM {$this -> valView} WHERE $field = :data AND id <> :id";
									$dataSQL['data'] = $data;
								}
							} else {
								// Solo podrá existir un registro $data por tabla BD pero si podrá existir varios registros NULL (Si lo permite la configuración de la BD)
								$dataSQL['data'] = $data;
								if ($ruleUnique['caseSensitive']) 
									$sql = "SELECT id FROM {$this -> valView} WHERE $field = _utf8 :data COLLATE utf8_bin AND id <> :id";
								else 
									$sql = "SELECT id FROM {$this -> valView} WHERE $field = :data AND id <> :id";	
							}
							
							// Si oneNull es false y el $data es NULL, entonces esta validación no se tomará en cuenta y terminará como satisfactoria

							if (!is_null($sql))
								$error = Validate::db_unique($sql, $dataSQL);
						} else throw new \Exception('No se encontró el ID de referencía para realizar la busqueda de unicos');
						break;
				}

				if (!is_null($error)) {
					if ($nextVal) $nextVal = false;

					$this -> valErrorsFields[$key] = $error;
				}
			}

		}

		return $nextVal;
	}

	private function val_doctrine_unique () {
		$nextVal = true;
		
		$entityManager = DoctrineORM::init() -> getEntityManager();
		
		return $nextVal;
	}

	/**
	 * Función que realizará la validación de servidor "Existencia" para cada campo de regla
	 * 
	 */
	private function val_db_exists () {
		$nextVal = true;

		foreach ($this -> valData as $key => $data) {
			$error = NULL;
			$ruleExists = $this -> valRules[$key] -> get_exists();
			$sql = NULL;

			$dataSQL = array(
				'data' => NULL,
				'id' => NULL
			);
			$error = NULL;

			$field = NULL;

			if (count($ruleExists) === 1)
				$field = $key;
			else if (count($ruleExists) === 2)
				$field = $ruleExists['field'];

			if (!is_null($field)) {
				if ($ruleExists['withID'] === -1) {
					if (is_null($data)) 
						$sql = "SELECT id FROM {$this -> valView} WHERE $field IS NULL";
					else {
						$sql = "SELECT id FROM {$this -> valView} WHERE $field = _utf8 :data COLLATE utf8_bin";
	
						$dataSQL['data'] = $data;
					}
	
					if (!is_null($sql)) 
						$error = Validate::db_exists($sql, $dataSQL);
				} else {
					$dataSQL['id'] = $ruleExists['withID'];
	
					if (is_null($data)) 
						$sql = "SELECT id FROM {$this -> valView} WHERE $field IS NULL AND id = :id";
					else {
						$dataSQL['data'] = $data;
	
						$sql = "SELECT id FROM {$this -> valView} WHERE $field = _utf8 :data COLLATE utf8_bin AND id = :id";
					}
	
					if (!is_null($sql))
						$error = Validate::db_exists($sql, $dataSQL);
				}
	
				if (!is_null($error)) {
					if ($nextVal) $nextVal = false;
					
					$this -> valErrorsFields[$key] = $error;
				}
			}

		}

		return $nextVal;
	}

	private function val_doctrine_exists () {
		echo "Doctrine Exists";
	}

	/**
	 * Función que valida la existencía del campo en un registro dado
	 *
	 */
	private function val_duplicate () {
		$nextVal = true;

		$dbReg = Validate::db_getReg($this -> valData['id'], $this -> valView);

		$dataSame = 0;

		foreach ($this -> valData as $key => $value) {
			// Se usa == en vez de === debido a que todos los datos de oldReg se encuentran en formato STRING, por lo que la ID y otros no se pueden comparar con los ID de $dataTreat (estos últimos se encuentran en formato NUMBER)
			$keyReg = $key;

			if (array_key_exists('field', $this -> valRules[$key] -> get_unique ()))
				$keyReg = $this -> valRules[$key] -> get_unique()['field'];
			else if (array_key_exists('field', $this -> valRules[$key] -> get_exists()))
				$keyReg = $this -> valRules[$key] -> get_exists()['field'];

			if ($dbReg[Text::toSnakeCase($keyReg)] == $value) $dataSame++;
		}

		
		if ($dataSame === count($this -> valData))  {
			$this -> valErrorMain = 'No se ha realizado ningún cambio con respecto al registro original. Imposible de progresar';
			
			if($nextVal) $nextVal = false;
		}
		
		return $nextVal;
	}

	public function validate () {
		if ($this -> valMethod === 'put')
			if (!$this -> val_duplicate()) return false;

		if ($this -> val_basic())
			if ($this -> val_image()) {
				if (empty(USE_ORM)) {
					if ($this -> val_db_unique())
						if ($this -> val_db_exists()) return true;
				} else {
					if ($this -> val_doctrine_unique())
						if ($this -> val_doctrine_exists()) return true;
				}
			}

		return false;
	}

}
