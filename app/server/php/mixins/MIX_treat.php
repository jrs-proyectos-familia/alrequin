<?php
namespace app\server\php\mixins;

use app\server\php\ini\mwtools\Treatment;

class Treat {

	private $rules = NULL;
	private $data = NULL;

	public function __construct (array $treat) {
		if (array_key_exists('rules', $treat))
			$this -> rules = $treat['rules'];
		else throw new \Exception('No se pudo encontrar las rules del treat');
		
		if (array_key_exists('data', $treat))
			$this -> data = $treat['data'];
		else throw new \Exception('No se pudo encontrar el data del treat');
	}

	public function treatment () {
		$dataTreat = array();

		foreach ($this -> data as $key => $data) {
			$dataTreat[$key] = $data;

			// Tratamiento de los strings
			if (gettype($data) === 'string') {
				if ($this -> rules[$key] -> get_trim())
					$dataTreat[$key] = Treatment::Trim($dataTreat[$key]);

				if ($this -> rules[$key] -> get_special_characters())
					$dataTreat[$key] = Treatment::SpecialCharacters($dataTreat[$key]);
			
				$dataTreat[$key] = Treatment::Text($dataTreat[$key], $this -> rules[$key] -> get_text());
			}

			// Tratamiento de las imagenes
			$treatRuleImage = $this -> rules[$key] -> get_image_route(); 
			
			if (count($treatRuleImage))
				$dataTreat[$key] = Treatment::ImageRoute($treatRuleImage['folder'], $treatRuleImage['name'], $treatRuleImage['type']);
		}
	
		return $dataTreat;
	}

}
