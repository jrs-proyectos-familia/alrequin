<?php
namespace app\server\php\middleware;

use config\php\db\DB;
use config\php\auth\Auth as AuthTools;
use config\php\exceptions\AuthException;

class AuthMiddleware {

	private $reqData = NULL;
	private $authChest = NULL;

	private $session = NULL;

	private $model = NULL;

	public function middleware ($req, $res, $next) {
		$this -> reqData = $req -> memory;

		try {
			$this -> session = AuthTools::check($this -> reqData['auth']);
			
			if ($this -> session['status']) {
				// TODO: Hacer más flexible este apartado
				$userClass = new \ReflectionClass('app\server\php\modules\users\data\Data');
				$this -> model = $userClass -> newInstance();
	
				foreach($this -> session['userToken'] as $key => $value) {
					if (method_exists($this -> model, "set_$key")) {
						$userClass -> getMethod("set_$key") -> invoke($this -> model, $value);
					} else throw new \Exception("method wasn't found");
				}
				
				$this -> authChest['session']['user'] = $this -> model;
	
				$req -> set_merge($this -> authChest);
				
				$next -> mw;
			} else $res -> code(401);
		} catch (AuthException $e) {
			if ($e instanceof AuthException)
				if ($e -> getCode() === 'ATE-200') $res -> code(401);

			throw $e;
		}
	}
}
