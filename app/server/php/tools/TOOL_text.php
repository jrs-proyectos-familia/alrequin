<?php
namespace app\server\php\tools;

class Text {

	/**
	 * Función que devuelve una cadena aleatoria de tamaño deseado
	 *
	 * @param [Int] $size
	 * @return String
	 */
	public static function chainRandom (int $size) {
		$chain = '';
		$characters = 'asdlkjhfg0192837465pqowieurytmznxcbvPASOIDUFYGTHRJEKWLQZMNXBCV0192837645';
		
		for ($i = 0; $i < $size; $i++)
			$chain.= $characters[rand(0, strlen($characters) -1)];

		return $chain;
	}

	public static function toSnakeCase(string $input) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}
		return implode('_', $ret);
	}

	public static function cutString (string $string, int $size) {
		$newString = "";

		if (strlen($string) > $size) {
			for($i = 0; $i < $size; $i++) $newString .= $string[$i];
			
			$newString .= '...';
		} else $newString = $string;

		return $newString;
	}

	
	/**
	 * Función que reemplaza la primera coincidencía de una cadena específica usando Regex
	 *
	 * @param string $search Cadena a buscar
	 * @param string $change Nueva cadena a reemplazar
	 * @param string $content Cadena raíz a ser modificada
	 * @return string Nueva cadena con el reemplazo realizado
	 */
	public static function replaceFirst (string $search, string $change, string $content) {
		$search = '/'.preg_quote($search, '/').'/';

		return preg_replace($search, $change, $content, 1);
	}

}
