<?php
namespace app\server\php\tools;

use config\php\exceptions\DataManageException;

use app\server\php\ini\annotations\Reader;

class DataManage {
	/**
	 * Función que enlaza los datos del cliente (frontend) con los datos 100% validos del servidor
	 *
	 * @param [string] $view El nombre de la vista en la cual se esta trabajando
	 * @param [array] $data Los datos json provenientes del cliente
	 * @param [array] $validData Los datos admitidos por el servidor
	 * @return array
	 */
	public static function linkData ($view, $data, $validData) {
		$mwtools = array(
			'treat' => array(
				'rules' => array(),
				'data' => array()
			),
			'val' => array(
				'rules' => array()
			),
			'serv' => array(
				'model' => NULL
			)
		);

		if (empty($data))
			return $mwtools; 

		foreach($data as $key => $value) {
			if (in_array($key, $validData, true)) {
				$mwtools['treat']['data'][$key] = $value;

				$ruleTreat = new \ReflectionClass('app\server\php\modules\\'.$view.'\rules\\'.$key.'\RULE_TREAT');
				$mwtools['treat']['rules'][$key] = $ruleTreat -> newInstance();

				// Actualizar con los valores predeterminados de las anotaciones a las reglas por default de las rules (RULES_TREAT)
				$annotations = Reader::annotationsClass($ruleTreat);
				foreach($annotations as $ann) {
					if ($ann instanceof \app\server\php\ini\annotations\Rules_Treat) {
						if ($mwtools['treat']['rules'][$key] -> get_trim() !== $ann -> use_trim)
							$mwtools['treat']['rules'][$key] -> treat_trim($ann -> use_trim);

						if ($ann -> transform_text !== 'skip')
							$mwtools['treat']['rules'][$key] -> treat_text($ann -> transform_text);

						if ($mwtools['treat']['rules'][$key] -> get_special_characters() !== $ann -> remove_special_characters)
							$mwtools['treat']['rules'][$key] -> treat_special_characters($ann -> remove_special_characters);
					}
				}

				$ruleVal = new \ReflectionClass('app\server\php\modules\\'.$view.'\rules\\'.$key.'\RULE_VAL');
				$mwtools['val']['rules'][$key] = $ruleVal -> newInstance();

				// Actualizar con los valores predeterminados de las anotaciones a las reglas por default de las rules (RULES_VAL)
				$annotations = Reader::annotationsClass($ruleVal);
				foreach ($annotations as $ann) {
					if ($ann instanceof \app\server\php\ini\annotations\Rules_Val) {
						if ($mwtools['val']['rules'][$key] -> get_empty() !== $ann -> no_empty)
							$mwtools['val']['rules'][$key] -> val_empty($ann -> no_empty);
							
						if ($mwtools['val']['rules'][$key] -> get_type() !== $ann -> data_type)
							$mwtools['val']['rules'][$key] -> val_type($ann -> data_type);

						if ($mwtools['val']['rules'][$key] -> get_min() !== $ann -> min_characters)
							$mwtools['val']['rules'][$key] -> val_min($ann -> min_characters);

						if ($mwtools['val']['rules'][$key] -> get_max() !== $ann -> max_characters)
							$mwtools['val']['rules'][$key] -> val_max($ann -> max_characters);

						if ($mwtools['val']['rules'][$key] -> get_bigger() !== $ann -> value_bigger)
							$mwtools['val']['rules'][$key] -> val_bigger($ann -> value_bigger);

						if ($mwtools['val']['rules'][$key] -> get_lower() !== $ann -> value_lower)
							$mwtools['val']['rules'][$key] -> val_lower($ann -> value_lower);
					}
				}
			}
		}

		return $mwtools;
	}

	/**
	 * Método que retorna el enlaze idóneo de las rules de los datos (campos) especificos.
	 *
	 * @param string $nameInitClass Nombre inicial para alcanzar la clase del dato deseado
	 * @return array Arreglo de instancías las 2 clases de rules para dicho objeto RULE_TREAT, RULE_VAL
	 */
	public static function linkData_Rules(string $nameInitClass) {
		$success = false;
		
		// Checamos si existen las clases correspondientes
		if (class_exists($nameInitClass.'\RULE_TREAT'))
			if (class_exists($nameInitClass.'\RULE_VAL'))
				$success = true;

		if ($success) {
			$instances = array(
				"treat" => NULL,
				"val" => NULL
			);

			$reflectionTreat = new \ReflectionClass($nameInitClass.'\RULE_TREAT');
			$instances['treat'] = $reflectionTreat -> newInstance();
			$annotations = Reader::annotationsClass($reflectionTreat);
			foreach($annotations as $ann) {
				if ($ann instanceof \app\server\php\ini\annotations\Rules_Treat) {
					if ($instances['treat'] -> get_trim() !== $ann -> use_trim)
							$instances['treat'] -> treat_trim($ann -> use_trim);

					if ($ann -> transform_text !== 'skip')
						$instances['treat'] -> treat_text($ann -> transform_text);

					if ($instances['treat'] -> get_special_characters() !== $ann -> remove_special_characters)
						$instances['treat'] -> treat_special_characters($ann -> remove_special_characters);
				}
			}

			$reflectionVal = new \ReflectionClass($nameInitClass.'\RULE_VAL');
			$instances['val'] = $reflectionVal -> newInstance();
			$annotations = Reader::annotationsClass($reflectionVal);
			foreach($annotations as $ann) {
				if ($ann instanceof \app\server\php\ini\annotations\Rules_Val) {
					if ($instances['val'] -> get_empty() !== $ann -> no_empty)
						$instances['val'] -> val_empty($ann -> no_empty);
						
					if ($instances['val'] -> get_type() !== $ann -> data_type)
						$instances['val'] -> val_type($ann -> data_type);

					if ($instances['val'] -> get_min() !== $ann -> min_characters)
						$instances['val'] -> val_min($ann -> min_characters);

					if ($instances['val'] -> get_max() !== $ann -> max_characters)
						$instances['val'] -> val_max($ann -> max_characters);

					if ($instances['val'] -> get_bigger() !== $ann -> value_bigger)
						$instances['val'] -> val_bigger($ann -> value_bigger);

					if ($instances['val'] -> get_lower() !== $ann -> value_lower)
						$instances['val'] -> val_lower($ann -> value_lower);
				}
			}

			if (!is_null($instances['treat']) && !is_null($instances['val']))
				return $instances;
			else throw new DataManageException('DME-200', $nameInitClass, "An error with the rules");
		} else throw new DataManageException('DME-100', $nameInitClass, "No found the class for $nameInitClass");
	}

	/**
	 * Función que genera la clase data del modelo deseado
	 *
	 * @param [string] $view El nombre de la vista en el que se esta trabajando
	 * @param [type] $dataTreat los datos ya tratados en cualquier ámbito
	 * @return object El módelo de los datos
	 */
	public static function modelData ($view, $dataTreat) {
		$classData = new \ReflectionClass('app\server\php\modules\\'.$view.'\data\DATA');

		$classInstance = $classData -> newInstance();
		
		foreach ($dataTreat as $key => $value) {
			if (isset($value) && !empty($value)) {
				if (method_exists($classInstance, "set_$key"))
					$classData -> getMethod('set_'.$key) -> invoke($classInstance, $value);
				else throw new DataManageException("DME-300", "set_$key", "No found the method set_$key");
			}
		}
		
		return $classInstance;
	}
}

