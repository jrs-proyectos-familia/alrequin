<?php
namespace app\server\php\tools;

use config\php\exceptions\FileException;

use const config\php\args\{MODE, CHMOD_CLIENT, DIR_STORE};

class Files {
	public static function removeDir (string $dir) {
		if (is_dir(DIR_STORE.$dir)) {
			$files = scandir(DIR_STORE.$dir);
			
			$emptyDir = true;

			foreach($files as $file) {
				if ($file !== '.' && $file !== '..') {
					$emptyDir = false;
					break;
				}
			}
			
			if ($emptyDir) rmdir(DIR_STORE.$dir);
			else throw new FileException('FLE-101', 'Directoy with files');
			
		} else throw new FileException('FLE-100', 'Directory no exists');
	}

	public static function saveFile (string $fileTmp, string $fileRoute) {
		$state = false;

		$fileExplode = explode('/', $fileRoute);
		array_pop($fileExplode);
		$dirFile = implode('/', $fileExplode);

		if (!is_dir(DIR_STORE.$dirFile)) {
			if (MODE === 'prod')
				$existFolder = mkdir(DIR_STORE.$dirFile, CHMOD_CLIENT);
			else {
				$oldMask = umask(0);
				$existFolder = mkdir(DIR_STORE.$dirFile, 0777);
				umask($oldMask);
			}
		}
		
		if (move_uploaded_file($fileTmp, DIR_STORE.$fileRoute)) $state = true;
		else Files::removeDir($dirFile);
		
		return $state;
	}

	public static function removeFile (string $fileRoute) {
		$state = false;

		if (is_file(DIR_STORE.$fileRoute))
			if (unlink(DIR_STORE.$fileRoute))
				$state = true;
			
		return $state;
	}
}
