<?php
namespace app\server\php\ini\annotations;

use Doctrine\Common\Annotations\AnnotationReader;
use config\php\exceptions\AnnReaderException;

class Reader {

	/**
	 * Método que devuelve una instancía del objeto AnnotationReader; con la cual se podrán leer las anotaciónes
	 *
	 * @return object AnnotationReader
	 */
	private static function classReader () {
		return new AnnotationReader();
	}

	/**
	 * Método que leerá las anotaciones escritas de cualquier clase ubicados en la parte superior del nombramiento de dicha clase
	 *
	 * @param object $reflectionClass Reflexión de la clase a leer sus anotaciones
	 * @return array Las anotaciones totales de dicha clase
	 */
	public static function annotationsClass (object $reflectionClass = null) {
		if (!empty($reflectionClass)) {
			try {
				return Reader::classReader() -> getClassAnnotations($reflectionClass);
			} catch (\Doctrine\Common\Annotations\AnnotationException $e) {
				throw new AnnReaderException("ANN_RDE-000", $e -> getMessage());
			}
		} else throw new AnnReaderException("ANN_RDE-100", "Required a reflection class");
	}
	
	/**
	 * Método que leerá la anotación especifica de cualquier clase ubicada en la parte superior del nombramiento de dicha clase
	 *
	 * @param object $reflectionClass Reflexión de la clase a leer la anotación especifica
	 * @param string $annotationName Nombre de la anotación a leer
	 * @return object La clase contenedora de la anotación deseada
	 */
	public static function annotationsClass_get (object $reflectionClass = null, string $annotationName = null) {
		if (!empty($reflectionClass))
			if (!empty($annotationName))
				return Reader::classReader() -> getClassAnnotation($reflectionClass, $annotationName);
			else throw new AnnReaderException("ANN_RDE-005", "Required an annotation class");
		else throw new AnnReaderException("ANN_RDE-100", "Required a reflection class");
	}

	/**
	 * Método que leerá las anotaciones escritas en cualquier propiedad dentro de una clase cualquiera
	 *
	 * @param object $reflectionProperty Reflexión de la propiedad a leer sus anotaciones
	 * @return array Las anotaciones totales de dicha propiedad
	 */
	public static function annotationsProperties (object $reflectionProperty = null) {
		if (!empty($reflectionProperty))
			return Reader::classReader() -> getPropertyAnnotations($reflectionProperty);
		else throw new AnnReaderException("ANN_RDE-200", "Required a reflection property");
	}
	
	/**
	 * Método que leerá la anotación especifica de cualquier propiedad dentro de una clase cualquiera
	 *
	 * @param object $reflectionClass Reflexión de la propiedad a leer su anotación específica
	 * @param string $annotationName Nombre de la anotación a leer
	 * @return object La clase contenedora de la anotación deseada
	 */
	public static function annotationsProperties_get (object $reflectionProperty = null, string $annotationName = null) {
		if (!empty($reflectionProperty))
			if (!empty($annotationName))
				return Reader::classReader() -> getPropertyAnnotation($reflectionProperty, $annotationName);
			else throw new AnnReaderException("ANN_RDE-005", "Required an annotation class");
		else throw new AnnReaderException("ANN_RDE-200", "Required a reflection property");
	}

}
