<?php
namespace app\server\php\ini\annotations;

use config\php\exceptions\AnnReaderException;
use config\php\exceptions\AnnRulesValException;

use app\server\php\ini\annotations\Reader;

/**
 * @Annotation
 * @Target("CLASS")
 * @Attributes({
 * 	@Attribute("no_empty", type="boolean"),
 * 	@Attribute("data_type", type="string"),
 * 	@Attribute("min_characters", type="integer"),
 * 	@Attribute("max_characters", type="integer"),
 * 	@Attribute("value_bigger", type="integer"),
 * 	@Attribute("value_lower", type="integer")
 * })
 */
class Rules_Val {

	public $no_empty;
	
	public $data_type;

	public $min_characters;

	public $max_characters;

	public $value_bigger = -1;

	public $value_lower = -1;

	/**
	 * Constructor del objeto Rules Val
	 */
	public function __construct (array $values) {
		if (array_key_exists("clone", $values))
			$this -> get_annotations_clone($values["clone"]);
		else {
			$this -> check_no_empty($values);
			$this -> check_data_type($values);
			$this -> check_min_characters($values);
			$this -> check_max_characters($values);
			$this -> check_value_bigger($values);
			$this -> check_value_lower($values);
		}
	}

	/**
	 * Método que se encarga de obtener las propiedades de las anotaciones heredadas
	 *
	 * @param string $namespace_class Nombre de la clase de la cual se copiaran las propiedades heredadas
	 */
	private function get_annotations_clone(string $namespace_class) {
		$reflection = new \ReflectionClass($namespace_class);
		$annotations = Reader::annotationsClass($reflection);

		if (!count($annotations)) throw new AnnReaderException("ANN_RDE-010", "Failed get inherited annotations");
		else {
			foreach ($annotations as $ann) {
				$nameClass = __CLASS__;

				if ($ann instanceof $nameClass) {
					$this -> no_empty = $ann -> no_empty;
					$this -> data_type = $ann -> data_type;
					$this -> min_characters = $ann -> min_characters;
					$this -> max_characters = $ann -> max_characters;
					$this -> value_bigger = $ann -> value_bigger;
					$this -> value_lower = $ann -> value_lower;
				}
			}
		}
	}

	/**
	 * Método que validará la propiedad no_empty
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_no_empty(array $values) {
		if (array_key_exists('no_empty', $values))
			$this -> no_empty = $values['no_empty'];
		else throw new AnnRulesValException("ANN_RVE-100", "It's necessary no_empty property");
	}

	/**
	 * Método que validará la propiedad data_type
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_data_type(array $values) {
		if (array_key_exists('data_type', $values)) {
			$isString = $values['data_type'] === 'string' ? true : false;
			$isInteger = $values['data_type'] === 'integer' ? true : false; 
			$isDecimal = $values['data_type'] === 'decimal' ? true : false; 
			$isEmail = $values['data_type'] === 'email' ? true : false; 

			if ($isString || $isInteger || $isDecimal || $isEmail)
				$this -> data_type = $values['data_type'];
			else throw new AnnRulesValException('ANN_RVE-201', "string, integer, decimal, email");

		} else throw new AnnRulesValException('ANN_RVE-200', "It's necessary data_type property");
	}

	/**
	 * Método que validará la propiedad min_characters
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_min_characters (array $values) {
		if (array_key_exists('min_characters', $values))
			$this -> min_characters = $values['min_characters'];
		else throw new AnnRulesValException('ANN_RVE-300', "It's necessary min_characters property");
	}

	/**
	 * Método que validará la propiedad max_characters
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_max_characters (array $values) {
		if (array_key_exists('max_characters', $values))
			$this -> max_characters = $values['max_characters'];
		else throw new AnnRulesValException('ANN_RVE-400', "It's necessary max_characters property");
	}

	/**
	 * Método que validará la propiedad value_bigger
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_value_bigger (array $values) {
		if (array_key_exists('value_bigger', $values))
			$this -> value_bigger = $values['value_bigger'];
		else {
			// No es obligatorio ingresar esta propiedad

			//throw new AnnRulesValException('ANN_RVE-500', "It's necessary value_bigger property");
		}
		
	}

	/**
	 * Método que validará la propiedad value_lower
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_value_lower (array $values) {
		if (array_key_exists('value_lower', $values))
			$this -> value_lower = $values['value_lower'];
		else {
			// No es obligatorio ingresar esta propiedad
			
			//throw new AnnRulesValException('ANN_RVE-600', "It's necessary value_lower property");
		}
	}

}
