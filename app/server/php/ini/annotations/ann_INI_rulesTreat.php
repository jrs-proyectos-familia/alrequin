<?php
namespace app\server\php\ini\annotations;

use config\php\exceptions\AnnReaderException;
use config\php\exceptions\AnnRulesTreatException;

use app\server\php\ini\annotations\Reader;

/**
 * @Annotation
 * @Target("CLASS")
 * @Attributes({
 * 	@Attribute("use_trim", type="boolean"),
 * 	@Attribute("transform_text", type="string"),
 * 	@Attribute("remove_special_characters", type="boolean")
 * })
 */
class Rules_Treat {

	public $use_trim;

	public $transform_text;

	public $remove_special_characters;

	/**
	 * Constructor del objeto Rules Treat
	 */
	public function __construct (array $values) {
		if (array_key_exists("clone", $values))
			$this -> get_annotations_clone($values["clone"]);
		else {
			$this -> check_use_trim($values);
			$this -> check_transform_text($values);
			$this -> check_remove_special_characters($values);		
		}
	}

	/**
	 * Método que se encarga de obtener las propiedades de las anotaciones heredadas
	 *
	 * @param string $namespace_class Nombre de la clase de la cual se copiaran las propiedades heredadas
	 */
	private function get_annotations_clone (string $namespace_class) {
		$reflection = new \ReflectionClass($namespace_class);
		$annotations = Reader::annotationsClass($reflection);
		
		if (!count($annotations)) throw new AnnReaderException("ANN_RDE-010", "Failed get inherited annotations");
		else {
			foreach ($annotations as $ann) {
				$nameClass = __CLASS__;
				
				if ($ann instanceof $nameClass) {
					$this -> use_trim = $ann -> use_trim;
					$this -> transform_text = $ann -> transform_text;
					$this -> remove_special_characters = $ann -> remove_special_characters;
				}
			}
		}
	}

	/**
	 * Método que validará la propiedad use_trim
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_use_trim (array $values) {
		if (array_key_exists("use_trim", $values))
			$this -> use_trim = $values["use_trim"];
		else throw new AnnRulesTreatException("ANN_RTE-100", "It's necessary use_trim property");
	}

	/**
	 * Método que validará la propiedad transform_text
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_transform_text (array $values) {
		if (array_key_exists("transform_text", $values)) {
			$isSkip = $values["transform_text"] === "skip" ? true : false;
			$isLowercase = $values["transform_text"] === "lowercase" ? true : false;
			$isUppercase = $values["transform_text"]  === "uppercase" ? true : false;
			$isCapitalize = $values["transform_text"] === "capitalize" ? true: false;
			
			if ( $isSkip || $isLowercase || $isUppercase || $isCapitalize )
				$this -> transform_text = $values["transform_text"];
			else throw new AnnRulesTreatException("ANN_RTE-201", "skip, lowercase, uppercase, capitalize");
			
		} else throw new AnnRulesTreatException("ANN_RTE-200", "It's necessary the transform_text property");
	}

	/**
	 * Método que validará la propiedad remove_special_characters
	 *
	 * @param array $values Array contenedora de las propiedades globales
	 */
	private function check_remove_special_characters (array $values) {
		if (array_key_exists("remove_special_characters", $values))
			$this -> remove_special_characters = $values["remove_special_characters"];
		else throw new AnnRulesTreatException("ANN_RTE-300", "It's necessary the remove_special_characters property");
		
	}

}
