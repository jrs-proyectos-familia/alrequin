<?php
namespace app\server\php\ini\mwtools;

use config\php\db\DB;

class Validate {

	public static function empty ($data) {
		if (isset($data) && !empty($data)) return NULL;
		return 'No puede estar vacio';
	}

	public static function type ($data, $option) {
		$error = NULL;

		switch ($option) {
			case 'string':
				if (gettype($data) !== 'string') $error = 'Debe de ser un texto';
				break;
			case 'integer':
				if (gettype($data) !== 'integer') $error = 'Debe de ser un numero';
				break;
			case 'decimal':
				if (gettype($data) !== 'double') $error = 'Debe de ser un decimal';
				break;
			case 'email':
				if (filter_var($data, FILTER_VALIDATE_EMAIL) === false) $error = 'Debe de ser un email Valido';
				break;
			default:
				$error = 'No se pudo identificar el tipo de dato';
		}

		return $error;
	}

	public static function min ($data, $min) {
		if (gettype($min) !== 'integer')
			throw new \Exception('Solo se permiten valores enteros para el valor del <b>Minímo</b>');
		
		$sizeData = strlen($data);

		if ($min > 0)
			if ($sizeData < $min) return 'Te faltan '.($min - $sizeData). ' carácteres';

		return NULL;
	}

	public static function max ($data, $max) {
		if (gettype($max) !== 'integer')
			throw new \Exception('Solo se permiten valores enteros para el valor del <b>Máximo</b>');
		
		$sizeData = strlen($data);

		if ($max > 0 && gettype($max) === 'integer')
			if ($sizeData > $max) return 'Te sobran '. ($sizeData - $max) . ' carácteres';

		return NULL;
	}

	public static function bigger ($data, $bigger) {
		if (gettype($bigger) !== 'integer')
			throw new \Exception("Solo se permiten valores enteros para el valor del <b>Bigger $bigger</b>");
		if (gettype($data) !== 'integer' && gettype($data) !== 'double')
			throw new \Exception("Solo se permiten valores enteros o decimales para el valor del <b>Data $data</b>");

		if ($bigger >= 0)
			if ($data <= $bigger) return 'Debe de ser mayor a '. $bigger;

		return NULL;
	}

	public static function lower ($data, $lower) {
		if (gettype($lower) !== 'integer')
			throw new \Exception("Solo se permiten valores enteros para el valor del <b>Lower $lower</b>");
		if (gettype($data) !== 'integer' && gettype($data) !== 'double')
			throw new \Exception("Solo se permiten valores enteros o decimales para el valor del <b>Data $data</b>");

		if ($lower > 0)
			if ($data >= $lower) return 'Debe de ser menor a '.$lower;

		return NULL;
	}

	/**
	 * Función que realiza la validación única de un registro (IMPORTANTE: Solo funcionará con 1 parametro de entrada)
	 *
	 * @param [string] $sql La sentencia sql a usar para la validación
	 * @param [string or number] $data Los datos necesarios para realizar la busqueda 
	 * @return boolean
	 */
	public static function db_unique ($sql, $data) {
		$error = NULL;
		
		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> prepare($sql);
			if (!is_null($data['data']))
				$stmt -> bindValue('data', $data['data']);
			if (!is_null($data['id']))
				$stmt -> bindValue('id', $data['id']);
			$stmt -> execute();
			$row = $stmt -> fetchAll();
			
			if (count($row) > 0) $error = 'Ya se encuentra en nuestro sistema... no se permiten duplicados';
		}

		return $error;
	}

	/**
	 * Función que realiza la validación de existencía de un registro (IMPORTANTE: Solo funcionará con 1 parametro de entrada)
	 *
	 * @param [string] $sql La sentencia sql a usar para la validación
	 * @param [string o number] $data Los datos necesarios para realizar la busqueda 
	 * @param [string] $bindValue Atributo bind del Sql en el cual será sustituido el valor $data
	 * @return boolean
	 */
	public static function db_exists ($sql, $data) {
		$error = NULL;
		
		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> prepare($sql);
			if (!is_null($data['data']))
				$stmt -> bindValue('data', $data['data']);
			if (!is_null($data['id']))
				$stmt -> bindValue('id', $data['id']);
			$stmt -> execute();
			$row = $stmt -> fetchAll();
			
			if (count($row) !== 1) $error = 'No se encuentra el registro deseado en nuestra base de datos';
		}

		return $error;
	}

	public static function db_getReg ($id, $view) {
		$oldReg = NULL;

		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> prepare('SELECT * FROM '.$view.' WHERE id = :id');
			$stmt -> bindValue('id', $id);
			$stmt -> execute();

			$oldReg = $stmt -> fetch(\PDO::FETCH_ASSOC);
		}

		return $oldReg;
	}

	public static function image_type ($data) {
		$error = 'Tu imagen es de tipo ['.explode('/', $data)[1].']; solo se permiten imagenes de tipo [JPEG, PNG o GIF]';

		$isJPEG = $data === 'image/jpeg';
		$isPNG = $data === 'image/png';
		$isGIF = $data === 'image/gif';

		if ($isJPEG || $isPNG  || $isGIF)
			$error = NULL;

		return $error;
	}

	public static function file_size ($data, $weightValid) {
		$error = NULL;

		if ($data > $weightValid)
			$error = 'Tu archivo es demasiado pesado para nuestros servidores. Solo podemos almacenar archivos cuyo peso máximo sea de '. (($weightValid * 9.7656) / 10000).' KB';

		return $error;
	}

}

class ValidateMethods {

	/**
	 * Función que realiza la validación básica de un campo
	 *
	 * @param [string/number] $data El valor del campo
	 * @param [string] $type El tipo de dato que es requerido para el campo
	 * @param [number] $min El número mínimo de carácteres necesarios
	 * @param [number] $max El número máximo de carácteres permitidos
	 * @return string del error presentado o NULL en caso de que todo haya salido bien
	 */
	public static function valEmptyTypeMinMax ($data, $type, $min, $max) {
		$error = Validate::empty($data);
		
		if ($error === NULL) {
			$error = Validate::type($data, $type);
			if ($error === NULL) {
				$error = Validate::min($data, $min);
				if ($error === NULL) {
					$error = Validate::max($data, $max);
					if ($error === NULL) return NULL;
				}
			}
		}

		return $error;
	}

	/**
	 * Función que realiza la validación básica de un campo; con la particularidad de que el campo es opciónal, es decir, puede estar o no estar vacio
	 *
	 * @param [string/number] $data El valor del campo
	 * @param [string] $type El tipo de dato que es requerido para el campo
	 * @param [number] $min El número mínimo de carácteres necesarios
	 * @param [number] $max El número máximo de carácteres permitidos
	 * @return string del error presentado o NULL en caso de que todo haya salido bien
	 */
	public static function valOptionalEmptyTypeMinMax ($data, $type, $min, $max) {
		$error = NULL;

		if (Validate::empty($data) === NULL) {
			$error = Validate::type($data, $type);
			if ($error === NULL) {
				$error = Validate::min($data, $min);
				if ($error === NULL) {
					$error = Validate::max($data, $max);
					if ($error === NULL) return NULL;
				}
			}
		}
		
		return $error;
	}

	/**
	 * Función que valida el estado de una imagen proveniente del cliente
	 *
	 * @param [array] $image Arreglo de la imagen contenedora de sus atributos a validar
	 * @return string del error presentado o NULL en caso de que todo haya salido bien
	 */
	public static function valImageFile($imageError, $imageType, $imageSize) {
		$error = NULL;

		if ($imageError)
			$error = 'Se encontró un error en la imagen a subir. Intenta cargar nuevamente la imagen o intenta subir otra imagen... Si persiste el error, por favor comunicalo a un staff de la plataforma en las redes sociales oficiales';
		else {
			$error = Validate::image_type($imageType);
			if ($error === NULL)
				$error = Validate::file_size($imageSize, 204800);
				if ($error === NULL)
					return NULL;
		}

		return $error;
	}

}
