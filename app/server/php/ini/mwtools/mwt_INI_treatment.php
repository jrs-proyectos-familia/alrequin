<?php
namespace app\server\php\ini\mwtools;

use app\server\php\tools\Text;

class Treatment {

	/**
	 * Función que eliminará todos los espacios vacios tanto al inicio como al final
	 *
	 * @param [string] $data
	 * @return string
	 */
	public static function Trim ($data) {
		if (isset($data) && !empty($data)) return trim($data);
		return $data;
	}

	/**
	 * Función que transformará los textos a una opción deseada
	 *
	 * @param [string] $data
	 * @param [string] $option Opción que definirá el texto al cual se va a transformar
	 * @return string
	 */
	public static function Text ($data, $option) {
		if (isset($data) && !empty($data) && gettype($data) === 'string') {
			switch ($option) {
				case 'lowercase':
					return strtolower($data);
				case 'uppercase':
					return strtoupper($data);
					break;
				case 'capitalize':
					$split = explode(" ", $data);
					$newData = '';

					foreach ($split as $part) {
						$lowerWord = strtolower($part);
						
						$newData = $newData.ucfirst($lowerWord).' ';
					}
					
					return self::Trim($newData);
					break;
				default:
					return $data;
					break;
			}
		}

		return $data;
	}

	/**
	 * Función que reemplazará y eliminará algunos caracteres especiales
	 *
	 * @param [string] $data
	 * @return string
	 */
	public static function SpecialCharacters ($data) {
		if (isset($data) && !empty($data) && gettype($data) === 'string') {
			$normalizeChars = array(
				'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
				'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
				'Ï'=>'I', 'Ñ'=>'NI', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
				'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
				'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
				'ï'=>'i', 'ð'=>'o', 'ñ'=>'ni', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
				'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
				'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',
			);

			$newData = strtr($data, $normalizeChars);
			
			$newData = str_replace(' ', '-', $newData);
			$newData = preg_replace('/[^A-Za-z0-9\-]/', '', $newData); 
			
			return str_replace('-', ' ', $newData);
		}

		return $data;
	}

	/**
	 * Función que Genera un nombre único para cada archivo de tipo imagen
	 *
	 * @param [string] $folder Nombre del folder en donde caerá la imagen
	 * @param [String] $name Nombre de la imagen proveniente del cliente
	 * @param [String] $type Tipo de formato de la imagen proveniente del cliente
	 * @return String
	 */
	public static function ImageRoute ($folder, $name, $type) {
		$routeImage = null;
		
		$nameImage = self::Trim($name);
		
		if (count(explode('/', $folder)) > 1) $folder = 'default';

		if (isset($nameImage) && !empty($nameImage)) {
			$routeImage = 'public/img/'.$folder.'/HS_';

			$nameImage = explode('.', $name)[0];

			// $routeImage.= $nameImage.'_'.time().'_'.Text::chainRandom(10);
			$routeImage.= time().'_'.Text::chainRandom(10);

			switch ($type) {
				case 'image/jpeg':
					$routeImage.= '.jpg';
					break;
				case 'image/png':
					$routeImage.= '.png';
					break;
				case 'image/gif':
					$routeImage.= '.gif';
					break;
				default:
					$routeImage = null;
			}
		}
		
		return $routeImage;
	}

}
