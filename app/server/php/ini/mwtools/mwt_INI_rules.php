<?php
namespace app\server\php\ini\mwtools;

use config\php\exceptions\RulesException;

/**
 * Función que devuelve algunos parametros útiles del debug file
 *
 * @return array Parametros diversos del debug
 */
function debugData () { // TODO: Analizar si realmente es necesario utilizar esta operación
	$debug = debug_backtrace();
	$file = explode(DIRECTORY_SEPARATOR, $debug[0]['file']);
	$isFileRule = array_pop($file);

	return array(
		"isFileRule" => $isFileRule,
	);
}

class TreatRules {
	private $trim = true;
	private $text = null;
	private $special_characters = false;

	// Estras
	private $image_route = array(); // Permite generar una nombre único para una imagen a subir

	public function treat_trim ($value) {
		if (gettype($value) === 'boolean')
			$this -> trim = $value;
		else throw new RulesException('RLE-100', strtoupper(__FUNCTION__), "Only boolean value is allowed");
	}
	public function get_trim () {
		return $this -> trim;
	}

	public function treat_text ($value) {
		if (gettype($value) === 'string')
			$this -> text = $value;
		else throw new RulesException('RLE-101', strtoupper(__FUNCTION__), "Only string value is allowed");
	}
	public function get_text () {
		return $this -> text;
	}

	public function treat_special_characters ($value) {
		if (gettype($value) === 'boolean')
			$this -> special_characters = $value;
		else throw new RulesException('RLE-102', strtoupper(__FUNCTION__), "Only boolean value is allowed");
	}
	public function get_special_characters () {
		return $this -> special_characters;
	}

	// Métodos Extras
	public function treat_image_route ($value) {
		$debugData = debugData();

		if ($debugData["isFileRule"] !== 'rules') {
			if (gettype($value) === 'array') {
				if (count($value) === 3) {
					if (array_key_exists('folder', $value) && array_key_exists('name', $value) && array_key_exists('type', $value)) {
						foreach ($value as $key => $data)
							if (gettype($data) !== 'string')
								throw new RulesException('RLE-151-B', strtoupper(__FUNCTION__), "Only string data for the parametters");
						
						$this -> image_route = $value;
					} else throw new RulesException('RLE-151-A1', strtoupper(__FUNCTION__), "Parametters: folder, name, type");
				} else throw new RulesException('RLE-151-A', strtoupper(__FUNCTION__), "Insufficient parametters");
			} else throw new RulesException('RLE-151', strtoupper(__FUNCTION__), "Only array value");
		} else throw new RulesException('RLE-150', strtoupper(__FUNCTION__), "Only works in Link and Rules Files");
	}
	public function get_image_route () {
		return $this -> image_route;
	}

}

class ValRules {
	private $empty = true;
	private $type = 'string';
	private $min = -1;
	private $max = -1;
	private $bigger = -1;
	private $lower = -1;

	// Extras
	private $image = array(); // Valida si la imagen contiene los requesitos necesarios para el backend
	private $unique = array(); // Busca sin "CASE SENSITVE" (no afecta mayúsculas ni minúsculas) si el campo es único en toda la base de datos
	private $exists = array(); // Busca con "CASE SENSITIVE" (afecta mayúsculas y minúsculas) si el campo existe en el registro (id) especificado

	public function val_empty ($value) {
		if (gettype($value) === 'boolean')
			$this -> empty = $value;
		else throw new RulesException('RLE-500', strtoupper(__FUNCTION__), "Only boolean value");
	}
	public function get_empty () {
		return $this -> empty;
	}

	public function val_type ($value) {
		if (gettype($value) === 'string')
			$this -> type = $value;
		else throw new RulesException('RLE-501', strtoupper(__FUNCTION__), "Only string value");	
	}
	public function get_type () {
		return $this -> type;
	}

	public function val_min ($value) {
		if (gettype($value) === 'integer')
			$this -> min = $value;
		else throw new RulesException('RLE-502', strtoupper(__FUNCTION__), "Only integer value");
	}
	public function get_min () {
		return $this -> min;
	}

	public function val_max ($value) {
		if (gettype($value) === 'integer')
			$this -> max = $value;
		else throw new RulesException('RLE-503', strtoupper(__FUNCTION__), "Only integer value");
	}
	public function get_max () {
		return $this -> max;
	}

	public function val_bigger ($value) {
		if (gettype($value) === 'integer')
			$this -> bigger = $value;
		else throw new RulesException('RLE-504', strtoupper(__FUNCTION__), "Only integer value");
	}
	public function get_bigger () {
		return $this -> bigger;
	}

	public function val_lower ($value) {
		if (gettype($value) === 'integer')
			$this -> lower = $value;
		else throw new RulesException('RLE-505', strtoupper(__FUNCTION__), "Only integer value");
	}
	public function get_lower () {
		return $this -> lower;
	}

	// Métodos Extras
	public function val_image ($value) {
		$debugData = debugData();

		if ($debugData["isFileRule"] !== 'rules') {
			if (gettype($value) === 'array') {
				if (count($value) === 3) {
					if (array_key_exists('fileError', $value) && array_key_exists('fileType', $value) && array_key_exists('fileSize', $value)) {
						foreach ($value as $key => $data) {
							if ($key === 'fileError') {
								if (gettype($data) !== 'boolean')
									throw new RulesException('RLE-551-A2', strtoupper(__FUNCTION__), "Only boolean data");
							} else if ($key === 'fileType') {
								if (gettype($data) !== 'string')
									throw new RulesException('RLE-551-A3', strtoupper(__FUNCTION__), "Only string data");
							} else if ($key === 'fileSize') {
								if (gettype($data) !== 'integer')
									throw new RulesException('RLE-551-A4', strtoupper(__FUNCTION__), "Only integer data"); 
							}
						}

						$this -> image = $value;
					} else throw new RulesException('RLE-551-A1', strtoupper(__FUNCTION__), "Parametters: fileError, fileType, fileSize");
				} else throw new RulesException('RLE-151-A', strtoupper(__FUNCTION__), "Insufficient parametters");
			} else throw new RulesException('RLE-151', strtoupper(__FUNCTION__), "Only array value");
		} else throw new RulesException('RLE-150', strtoupper(__FUNCTION__), "Only works in Link and Rules Files");
	}
	public function get_image () {
		return $this -> image;
	}

	public function val_unique ($value) {
		$debugData = debugData();

		if ($debugData["isFileRule"] !== 'rules') {
			if (gettype($value) === 'array') {
				if (count($value) === 2) {
					if (array_key_exists('oneNull', $value) && array_key_exists('caseSensitive', $value)) {
						foreach ($value as $key => $data) {
							switch ($key) {
								case 'oneNull':
									if (gettype($data) !== 'boolean')
										throw new RulesException('RLE-651-A2', strtoupper(__FUNCTION__), "Only boolean data");
									break;
								case 'caseSensitive':
									if (gettype($data) !== 'boolean')
										throw new RulesException('RLE-651-A3', strtoupper(__FUNCTION__), "Only boolean data");
									break;
							}
						}
							
						$this -> unique = $value;
					} else throw new RulesException('RLE-651-A1-01', strtoupper(__FUNCTION__), "Parametters: oneNull, caseSensitive");
				} else if (count($value) === 3) {
					if (array_key_exists('oneNull', $value) && array_key_exists('caseSensitive', $value) && array_key_exists('field', $value)) {
						foreach ($value as $key => $data) {
							switch ($key) {
								case 'oneNull':
									if (gettype($data) !== 'boolean')
										throw new RulesException('RLE-651-A2', strtoupper(__FUNCTION__), "Only boolean data");
									break;
								case 'caseSensitive':
									if (gettype($data) !== 'boolean')
										throw new RulesException('RLE-651-A3', strtoupper(__FUNCTION__), "Only boolean data");
									break;
								case 'field':
									if (gettype($data) !== 'string')
										throw new RulesException('RLE-651-A4', strtoupper(__FUNCTION__), "Alternative Column");
									break;
							}
						}
							
						$this -> unique = $value;
					} else throw new RulesException('RLE-651-A1', strtoupper(__FUNCTION__), "Parametters: oneNull, caseSensitive, field");
				} else throw new RulesException('RLE-151-A', strtoupper(__FUNCTION__), "Insufficient parametters");
			} else throw new RulesException('RLE-151', strtoupper(__FUNCTION__), "Only array value");
		} else throw new RulesException('RLE-150', strtoupper(__FUNCTION__), "Only works in Link and Rules Files");
	}
	public function get_unique () {
		return $this -> unique;
	}

	public function val_exists ($value) {
		$debugData = debugData();

		if ($debugData["isFileRule"] !== 'rules') {
			if (gettype($value) === 'array') {
				if (count($value) === 1) {
					if (array_key_exists('withID', $value)) {
						foreach ($value as $key => $data) {
							if ($key === 'withID') {
								if (gettype($data) !== 'integer')
									throw new RulesException('RLE-751-A2', strtoupper(__FUNCTION__), "Only integer data");
								
								if ($data === 0 || $data < -1)
									throw new RulesException('RLE-751-A2-01', strtoupper(__FUNCTION__), "n > 1: Use ID, -1: Don't use ID");
							}
						}

						$this -> exists = $value;
					} else throw new RulesException('RLE-751-A1-01', strtoupper(__FUNCTION__), "Parametters: withID");
				} else if (count($value) === 2) {
					if (array_key_exists('withID', $value) && array_key_exists('field', $value)) {
						foreach ($value as $key => $data) {
							if ($key === 'field') {
								if (gettype($data) !== 'string')
									throw new RulesException('RLE-651-A4', strtoupper(__FUNCTION__), "Alternative Column");
							} else if ($key === 'withID') {
								if (gettype($data) !== 'integer')
									throw new RulesException('RLE-751-A2', strtoupper(__FUNCTION__), "Only integer data");
								
								if ($data === 0 || $data < -1)
									throw new RulesException('RLE-751-A2-01', strtoupper(__FUNCTION__), "n > 1: Use ID, -1: Don't use ID");
							}
						}

						$this -> exists = $value;
					} else throw new RulesException('RLE-751-A1', strtoupper(__FUNCTION__), "Parametters: withID, field");
				}else throw new RulesException('RLE-151-A', strtoupper(__FUNCTION__), "Insufficient parametters"); 
			} else throw new RulesException('RLE-151', strtoupper(__FUNCTION__), "Only array value");
		} else throw new RulesException('RLE-150', strtoupper(__FUNCTION__), "Only works in Link and Rules Files");
	}
	public function get_exists () {
		return $this -> exists;
	}
}
