<?php
	use config\php\route\Route;
	use const config\php\args\ROUTES;

	Route::get(ROUTES['ERROR_404'], function ($req, $res) {
		$page = array(
			'title' => 'Error 404'
		);

		$res -> render = array('view' => $req -> data['view'], 'args' => $page);
	});
