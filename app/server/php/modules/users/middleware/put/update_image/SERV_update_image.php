<?php
namespace app\server\php\modules\users\middleware\put\update_image;

use config\php\db\DB;
use config\php\exceptions\FileException;
use config\php\exceptions\AuthException;

use app\server\php\tools\Files;
use app\server\php\modules\users\crud\Update as CRUD_Update;

class MW_SERV {
	private $reqData = NULL;
	private $model = NULL;

	private $DB_conn = NULL;

	private $transaction = false;
	private $exceptions = false;

	public function middleware ($req, $res) {
		$this -> reqData = $req -> memory;

		$this -> model = $this -> reqData['serv']['model'];

		$this -> DB_conn = DB::init() -> conn();
		
		$this -> DB_conn -> beginTransaction();
		
		$stmt = CRUD_Update::updateImageTransation($this -> DB_conn, $this -> model);
		
		try {
			if ($stmt -> execute()) {
				// Guardamos la nueva imagen en el servidor
				if (isset($this -> reqData['files']['image'])) {
					$saving = Files::saveFile(
						$this -> reqData['files']['image']['tmp_name'], 
						$this -> model -> get_image()
					);
					
					if ($saving) $this -> transaction = true;
				} else $this -> transaction = true; // La imagen es opcional por lo cual siempre procederá en cualquier caso
			}
			
			if ($this -> transaction) {
				$this -> DB_conn -> commit();

				// Una vez que se logre guardar la nueva imagen en la base de datos ahora si eliminamos la imagen anterior
				if (!is_null($this -> model -> get_image_store()))
					Files::removeFile($this -> model -> get_image_store());
				
				$res -> code(201, [
					'image' => $this -> model -> get_image()
				]);

			} else {
				$this -> DB_conn -> rollBack();

				if (!is_null($this -> model -> get_image()))
					Files::removeFile($this -> model -> get_image());

				throw new AuthException("ATE-301", "Couldn't update the user");
			}

		} catch (\Exception $e) {
			// Si surge cualquier error entonces hay que remover la imagen subida (si es que se subió)
			if (!is_null($this -> model -> get_image()))
				Files::removeFile($this -> model -> get_image());

			throw $e;
		}
	}
}
