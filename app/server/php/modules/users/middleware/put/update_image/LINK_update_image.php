<?php
namespace app\server\php\modules\users\middleware\put\update_image;

use app\server\php\tools\DataManage;

trait Link_Extras {
	public function Extra_Id () {
		$rulesId = DataManage::linkData_Rules('app\server\php\modules\users\rules\id');

		//		=> Treat
		$this -> linkChest['treat']['rules']['id'] = $rulesId['treat'];
		$this -> linkChest['treat']['data']['id'] = $this -> reqData['session']['user'] -> get_id();

		// 		=> Val
		$this -> linkChest['val']['rules']['id'] = $rulesId['val'];
	}

	public function Extra_Image () {
		$rulesImage = DataManage::linkData_Rules('app\server\php\modules\users\rules\image');

		//		=> Treat
		$this -> linkChest['treat']['rules']['image'] = $rulesImage['treat'];
		$this -> linkChest['treat']['data']['image'] = NULL;

			// 	=> Val
		$this -> linkChest['val']['rules']['image'] = $rulesImage['val'];
	}

	public function Extra_Image_Store () {
		//		=> Treat
		// TODO: Crear un método Treat para convertir "" en NULL
		if (empty($this -> linkChest['treat']['data']['image_store'])) $this -> linkChest['treat']['data']['image_store'] = NULL;
	}
}

trait Link_Rules {
	public function Rule_Id () {
		$this -> linkChest['val']['rules']['id'] -> val_exists(array('withID' => -1));
	}

	public function Rule_Image () {
		//		=> Val
		$this -> linkChest['val']['rules']['image'] -> val_unique(array('oneNull' => false, 'caseSensitive' => false));

		if (isset($this -> reqData['files']['image'])) {
			//	=> Treat
			$this -> linkChest['treat']['rules']['image'] -> treat_image_route(array(
				'folder' => 'profile',
				'name' => $this -> reqData['files']['image']['name'],
				'type' => $this -> reqData['files']['image']['type']
			));
			
			//	=> Val
			$this -> linkChest['val']['rules']['image'] -> val_image(array(
				'fileError' => $this -> reqData['files']['image']['error'] ? true : false,
				'fileType' => $this -> reqData['files']['image']['type'],
				'fileSize' => $this -> reqData['files']['image']['size']
			));
		}
	}

	public function Rule_Image_Store () {
		$this -> linkChest['val']['rules']['image_store'] -> val_exists(array(
			'withID' => $this -> reqData['session']['user'] -> get_id(),
			'field' => 'image'
		));
	}
}

class MW_LINK {

	use Link_Extras;
	use Link_Rules;
	
	const validData = array(
		'image_store'
	);

	private $reqData = NULL;
	private $linkChest = NULL;

	public function middleware ($req, $res, $next) {
		$this -> reqData = $req -> memory;

		$this -> linkChest = DataManage::linkData($this -> reqData['view'], $this -> reqData['data'], self::validData);

		if (count($this -> linkChest['treat']['data']) === count(self::validData)) {
			// 						=== LINK DATA EXTRAS ===
			$this -> Extra_Id();
			$this -> Extra_Image();
			$this -> Extra_Image_Store();

			// 						=== UPDATE RULES ===
			$this -> Rule_Id();
			$this -> Rule_Image();
			$this -> Rule_Image_Store();
			
			$req -> set_merge($this -> linkChest);

			$next -> mw;
		} else $res -> code(406, ['error' => 'Hubo un error con los datos recibidos :(']); 
	}
}
