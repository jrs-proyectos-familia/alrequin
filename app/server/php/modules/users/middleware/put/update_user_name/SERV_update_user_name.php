<?php
namespace app\server\php\modules\users\middleware\put\update_user_name;

use config\php\db\DB;
use config\php\auth\Auth;
use config\php\exceptions\AuthException;

use app\server\php\modules\users\crud\{Update as CRUD_Update, Read as CRUD_Read};

class MW_SERV {

	private $reqData = NULL;
	private $model = NULL;

	private $DB_conn = NULL;
	
	private $session = array(
		'user' => NULL,
		'token' => NULL
	);
	
	private $transaction = false;

	public function middleware ($req, $res) {
		$this -> reqData = $req -> memory;
		
		$this -> model = $this -> reqData['serv']['model'];
		
		$this -> DB_conn = DB::init() -> conn();
		
		$this -> DB_conn -> beginTransaction();

		$stmt = CRUD_Update::updateUserName_Transaction($this -> DB_conn, $this -> model);
		
		if ($stmt -> execute()) {
			// AQUI VOY A BUSCAR EL NUEVO USUARIO
			$this -> session['user'] = CRUD_READ::read_user_by_id_Transaction($this -> DB_conn, $this -> model);
			
			$this -> session['token'] = Auth::generate($this -> session['user']);
			
			if ($this -> session['token'] !== NULL) $this -> transaction = true;
		}
		
		if ($this -> transaction) {
			$this -> DB_conn -> commit();

			$res -> code(201, [
				'token' => $this -> session['token'],
				'user' => array(
					'name' => $this -> session['user'] -> get_name(),
					'surname' => $this -> session['user'] -> get_surname(),
					'second_surname' => $this -> session['user'] -> get_second_surname(),
					'alias' => $this -> session['user'] -> get_alias(),
					'image' => $this -> session['user'] -> get_image()
				)
			]);
		} else {
			$this -> DB_conn -> rollBack();
			
			throw new AuthException("ATE-301", "Couldn't update the user");
		}
	}
}
