<?php
namespace app\server\php\modules\users\middleware\put\update_user_name;

use app\server\php\tools\DataManage;

trait Link_Extras {
	public function Extra_Id () {
		$rulesId = DataManage::linkData_Rules('app\server\php\modules\users\rules\id');

		//		=> Treat
		$this -> linkChest['treat']['rules']['id'] = $rulesId['treat'];
		$this -> linkChest['treat']['data']['id'] = $this -> reqData['session']['user'] -> get_id();
		
		// 		=> Val
		$this -> linkChest['val']['rules']['id'] = $rulesId['val'];
	}
}

trait Link_Rules {
	public function Rule_Id () {
		$this -> linkChest['val']['rules']['id'] -> val_exists(array('withID' => -1));
	}
}

class MW_LINK {
	
	use Link_Extras;
	use Link_Rules;

	const validData = array(
		'name',
		'surname',
		'second_surname'
	);

	private $reqData = NULL;
	private $linkChest = NULL;

	public function middleware ($req, $res, $next) {
		$this -> reqData = $req -> memory;

		$this -> linkChest = DataManage::linkData($this -> reqData['view'], $this -> reqData['data'], self::validData);

		if (count($this -> linkChest['treat']['data']) === count(self::validData)) {
			// 						=== LINK DATA EXTRAS ===
			$this -> Extra_Id();

			// 						=== UPDATE RULES ===
			$this -> Rule_Id();

			$req -> set_merge($this -> linkChest);

			$next -> mw;
		} else $res -> code(406, ['error' => 'Hubo un error con los datos recibidos :(']);
	}
}
