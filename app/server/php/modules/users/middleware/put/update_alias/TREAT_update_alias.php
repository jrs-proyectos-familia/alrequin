<?php
namespace app\server\php\modules\users\middleware\put\update_alias;

use app\server\php\mixins\Treat;

class MW_TREAT {

	private $reqData = NULL;
	private $treatChest = NULL;

	public function middleware ($req, $res, $next) {
		$this -> reqData = $req -> memory;

		if (count($this -> reqData['treat']['data'])) {
			$Treat = new Treat($this -> reqData['treat']);
			
			$this -> treatChest = $Treat -> treatment();

			$req -> set_key('treat\data', $this -> treatChest);

			$next -> mw;
		} else $res -> code(500, ['error' => 'No pasó el tratamiento de los datos']); 
	}
}
