<?php
namespace app\server\php\modules\users\middleware\put\update_alias;

use app\server\php\tools\DataManage;
use app\server\php\mixins\Val;

class MW_VAL {

	private $reqData = NULL;
	private $valChest = NULL;

	public function middleware ($req, $res, $next) {
		$this -> reqData = $req -> memory;

		$Val = new Val(
			$this -> reqData['treat']['data'], 
			$this -> reqData['val']['rules'], 
			$this -> reqData['method'], 
			$this -> reqData['view']
		);

		if ($Val -> validate()) {
			$this -> valChest = DataManage::modelData($this -> reqData['view'], $this -> reqData['treat']['data']);

			$req -> set_key('serv\model', $this -> valChest);

			$next -> mw;
		} else {
			if (!is_null($Val -> valErrorMain)) $res -> code(500, ['error' => $Val -> valErrorMain]);
			else $res -> code(500, ['error' => $Val -> valErrorsFields]);
		}

	}
}
