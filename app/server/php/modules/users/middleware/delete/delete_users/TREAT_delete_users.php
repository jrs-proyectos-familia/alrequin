<?php
namespace app\server\php\modules\users\middleware\delete\delete_user;

use function app\server\php\tools\RawData;

use app\server\php\mixins\Treat;
use app\server\php\tools\Treatment;

class MW_TREAT {
	public function __construct () {
		return function ($req, $res, $next) {
			$validData = array(
				'id',
				'image'
			);
			$dataTVS = RawData('user', $req['data'], $validData);
	
			if (count($dataTVS['dataTreat']) === count($validData)) {
				$req['mwTools'] = $dataTVS;
	
				$Treat = new Treat($req['mwTools']);
	
				$req['mwTools']['dataTreat'] = $Treat -> newTreat;
	
				$next -> invoke($req);
			} else $res['code'] -> invoke(406, array('error' => 'Hubo un error con los datos recibidos :('));
		};
	}
}
