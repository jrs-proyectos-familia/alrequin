<?php
namespace app\server\php\modules\users\middleware\delete\delete_user;

use function app\server\php\tools\ModelData;

use app\server\php\mixins\Val;

class MW_VAL {
	public function __construct () {
		return function ($req, $res, $next) {
			$success = true;
	
			$Val = new Val($req['mwTools'], $req['method']);
	
			$req['mwTools']['dataErrors'] = $Val -> newErrors;
	
			foreach ($req['mwTools']['dataErrors'] as $error) {
				if ($error !== NULL) {
					$success = false;
					break;
				}
			}
	
			if ($success) {
				$req['mwTools']['dataModel'] = ModelData('user', $req['mwTools']['dataTreat']);
	
				$next -> invoke($req);
			} else $res['code'] -> invoke(500, array('error' => $req['mwTools']['dataErrors']));
		};
	}
}
