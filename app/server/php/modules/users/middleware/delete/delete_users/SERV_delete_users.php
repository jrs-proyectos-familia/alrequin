<?php
namespace app\server\php\modules\users\middleware\delete\delete_user;

use config\php\db\DB;

use function app\server\php\mixins\deleteImage as RemovingImage;

use app\server\php\modules\users\crud\user as CRUD_user;

class MW_SERV {
	public function __construct () {
		return function ($req, $res) {
			$successService = false;
	
			$conn = DB::init() -> conn();
			
			try {
				$conn -> beginTransaction();
	
				$stmt = CRUD_user::deleteImageTransation($conn, $req['mwTools']['dataModel']);
	
				if ($stmt -> execute()) {
					if (!empty($req['mwTools']['dataModel'] -> get_image())) {
						if (RemovingImage($req['mwTools']['dataModel'] -> get_image()))
							$successService = true;
					} else $successService = true; // Esto es valido debido a que es opcional la imagen
				}
	
			} catch (\PDOException $e) {
				echo 'Hubo un error en el crud de eliminación del usuario';
			} finally {
				DB::destroy();
	
				if ($successService) {
					$conn -> commit();
	
					$res['code'] -> invoke(204);
				} else {
					$conn -> rollBack();
	
					$res['code'] -> invoke(500, array('error' => 'Hubo un error en el servicio de eliminación del usuario'));
				}
			}
		};
	}
}
