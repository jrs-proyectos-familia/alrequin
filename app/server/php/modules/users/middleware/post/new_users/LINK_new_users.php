<?php
namespace app\server\php\modules\users\middleware\post\new_users;

use app\server\php\tools\DataManage;

trait Link_Extras {
	public function Extra_Image () {
		$rulesImage = DataManage::linkData_Rules('app\server\php\modules\users\rules\image');
		
		//		=> Treat
		$this -> linkChest['treat']['rules']['image'] = $rulesImage['treat'];
		$this -> linkChest['treat']['data']['image'] = NULL;
		
		// 	=> Val
		$this -> linkChest['val']['rules']['image'] = $rulesImage['val'];
	}
}

trait Link_Rules {
	public function Rule_Alias () {
		$this -> linkChest['val']['rules']['alias'] -> val_unique(array('oneNull' => false, 'caseSensitive' => false));
	}

	public function Rule_Image () {
		//		=> Val
		$this -> linkChest['val']['rules']['image'] -> val_unique(array('oneNull' => false, 'caseSensitive' => false));

		if (isset($this -> reqData['files']['image'])) {
			// 		=> Treat
			$this -> linkChest['treat']['rules']['image'] -> treat_image_route(array(
				'folder' => 'profile', 
				'name' => $this -> reqData['files']['image']['name'], 
				'type' => $this -> reqData['files']['image']['type']
			));
			
			// 		=> Val
			$this -> linkChest['val']['rules']['image'] -> val_image(array(
				'fileError' => $this -> reqData['files']['image']['error'] ? true : false,
				'fileType' => $this -> reqData['files']['image']['type'],
				'fileSize' => $this -> reqData['files']['image']['size']
			));
		}
	}
}


class MW_LINK {

	use Link_Extras;
	use Link_Rules;

	const validData = array(
		'name',
		'surname',
		'second_surname',
		'alias'
	);

	private $reqData = NULL;
	private $linkChest = NULL;

	public function middleware ($req, $res, $next) {
		$this -> reqData = $req -> memory;
		
		$this -> linkChest = DataManage::linkData($this -> reqData['view'], $this -> reqData['data'], self::validData);
		
		if (count($this -> linkChest['treat']['data']) === count(self::validData)) {
			// 						=== LINK DATA EXTRAS ===
			$this -> Extra_Image();

			// 						=== UPDATE RULES ===
			$this -> Rule_Alias();
			$this -> Rule_Image();

			$req -> set_merge($this -> linkChest);
			
			$next -> mw;
		} else $res -> code(406, ['error' => 'Hubo un error con los datos recibidos :(']);
	}
}
