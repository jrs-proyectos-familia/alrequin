<?php
namespace app\server\php\modules\users\middleware\post\new_users;

use config\php\db\DB;
use config\php\exceptions\AuthException;
use config\php\exceptions\ExceException;

use app\server\php\tools\Files;
use app\server\php\modules\users\crud\Create as CRUD_Create;

class MW_SERV {

	private $reqData = NULL;
	private $model = NULL;

	private $DB_conn = NULL;

	private $transaction = false;
	private $exceptions = false;

	public function middleware ($req, $res) {
		$this -> reqData = $req -> memory;

		$this -> model = $this -> reqData['serv']['model'];

		$this -> DB_conn = DB::init() -> conn();

		$this -> DB_conn -> beginTransaction();
		
		$stmt = CRUD_Create::create_Transaction($this -> DB_conn, $this -> model);
		
		try {
			if ($stmt -> execute()) {	
				// Aqui comienza el guardado del archivo de la imagen en el servidor
				if (!empty($this -> model -> get_image())) {
					$saving = Files::saveFile(
						$this -> reqData['files']['image']['tmp_name'],
						$this -> model -> get_image()
					);
	
					if ($saving) $this -> transaction = true;
				} else $this -> transaction = true; // Esto es valido debido a que la imagen del usuario es opcional 
			}
			
			if ($this -> transaction) {
				$this -> DB_conn -> commit();
				
				$res -> code(204);
			} else {
				$this -> DB_conn -> rollBack();
	
				if (!is_null($this -> model -> get_image()))
					Files::removeFile($this -> model -> get_image());
	
				throw new AuthException("ATE-300", "Couldn't save the user");
			}
		} catch (\Exception $e) {
			// Si surge cualquier error entonces hay que remover la imagen subida (si es que se subió)
			if (!is_null($this -> model -> get_image()))
				Files::removeFile($this -> model -> get_image());

			throw $e;
		}
	}
}
