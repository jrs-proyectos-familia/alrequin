<?php
namespace app\server\php\modules\users\entity;

use Doctrine\ORM\Mapping\{
	Entity,
	Table,
	Id, GeneratedValue,
	Column
};

/**
 * @Entity
 * @Table(name="users")
 */
class Users {

	use UsersGet;
	use UsersSet;

	/**
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 * @var int
	 */
	private $id;

	/**
	 * @Column(length=25)
	 * @var string
	 */
	private $name;

	/**
	 * @Column(length=15)
	 * @var string
	 */
	private $surname;

	/**
	 * @Column(length=15)
	 * @var string
	 */
	private $second_surname;

	/**
	 * @Column(length=15, unique=true)
	 * @var string
	 */
	private $alias;

	/**
	 * @Column(unique=true, nullable=true)
	 * @var string
	 */
	private $image;
}

trait UsersGet {
	public function get_id () {
		return $this -> id;	
	}

	public function get_name () {
		return $this -> name;
	}

	public function get_surname () {
		return $this -> surname;
	}

	public function get_second_surname () {
		return $this -> second_surname;
	}

	public function get_alias () {
		return $this -> alias;
	}

	public function get_image () {
		return $this -> image;
	}
}

trait UsersSet {
	public function set_id (int $id) {
		$this -> id = $id;
	}

	public function set_name (string $name) {
		$this -> name = $name;
	}

	public function set_surname (string $surname) {
		$this -> surname = $surname;
	}

	public function set_second_surname (string $second_surname) {
		$this -> second_surname = $second_surname;
	}

	public function set_alias (string $alias) {
		$this -> alias = $alias;
	}

	public function set_image (string $image) {
		$this -> image = $image;
	}
}
