<?php
namespace app\server\php\modules\users\data;

use config\php\exceptions\DataException;

class Data {

	private $id = NULL;
	private $name = NULL;
	private $surname = NULL;
	private $second_surname = NULL;
	private $alias = NULL;
	private $image = NULL;

	// Auxiliares
	private $image_store = NULL;

	public function __construct() {}

	public function get_id () {
		return $this -> id;
	}
	public function set_id ($id) {
		if (gettype($id) === "integer")
			$this -> id = $id;
		else throw new DataException("DAT-100", __FUNCTION__, $id, "Only Integers");
	}

	public function get_name () {
		return $this -> name;
	}
	public function set_name ($name) {
		if (gettype($name) === 'string')
			$this -> name = $name;
		else throw new DataException("DAT-101", __FUNCTION__, $name, "Only Strings");
	}

	public function get_surname () {
		return $this -> surname;
	}
	public function set_surname ($surname) {
		if (gettype($surname) === 'string')
			$this -> surname = $surname;
		else throw new DataException("DAT-101", __FUNCTION__, $surname, "Only Strings");
	}

	public function get_second_surname() {
		return $this -> second_surname;
	}
	public function set_second_surname ($second_surname) {
		if (gettype($second_surname) === 'string')
			$this -> second_surname = $second_surname;
		else throw new DataException("DAT-101", __FUNCTION__, $second_surname, "Only Strings");
	}

	public function get_alias () {
		return $this -> alias;
	}
	public function set_alias ($alias) {
		if (gettype($alias) === 'string')
			$this -> alias = $alias;
		else throw new DataException("DAT-101", __FUNCTION__, $alias, "Only Strings");
	}

	public function get_image () {
		return $this -> image;
	}
	public function set_image ($image) {
		if (gettype($image) === 'string' || is_null($image))
			$this -> image = $image;
		else throw new DataException("DAT-201", $image, __FUNCTION__, "Only Strings or Null");
	}

	public function get_image_store () {
		return $this -> image_store;
	}
	public function set_image_store ($image_store) {
		if (gettype($image_store) === 'string' || is_null($image_store))
			$this -> image_store = $image_store;
		else throw new DataException("DAT-201", $image_store, __FUNCTION__, "Only Strings or Null");
	}

	public function __toString () {
		return <<< USER
		<h4>User</h4> 
		<ol>
				<li> <b>id: </b>$this->id</li>
				<li> <b>name: </b>$this->name</li>
				<li> <b>surname: </b>$this->surname</li>
				<li> <b>second_surname: </b>$this->second_surname</li>
				<li> <b>alias: </b>$this->alias</li>
				<li> <b>image: </b>$this->image</li>
				<li> <b>image_store: </b>$this->image_store</li>
		</ol>
		USER;
	}
}
