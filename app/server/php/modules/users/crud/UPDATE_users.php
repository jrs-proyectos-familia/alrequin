<?php
namespace app\server\php\modules\users\crud;

use config\php\db\DB;

use app\server\php\modules\users\crud\sql\Sql as SQL_User;

class Update {
	/**
	 * Método para actualizar el usuario en todos sus atributos
	 *
	 * @param [object] $user DataModel del usuario
	 * @return boolean True -> Usuario actualizado, False -> Falló en la actualización del usuario
	 */
	public static function update ($user) {
		$success = false;

		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::update);
			$stmt -> bindValue('id', $user -> get_id());
			$stmt -> bindValue('name', $user -> get_name());
			$stmt -> bindValue('surname', $user -> get_surname());
			$stmt -> bindValue('secondSurname', $user -> get_secondSurname());
			$stmt -> bindValue('alias', $user -> get_alias());
			$stmt -> bindValue('image', $user -> get_image());
			
			if ($stmt -> execute()) $success = true;
		}

		return $success;
	}

	/**
	 * Método para actualizar el nombre del usuario
	 *
	 * @param [object] $user DataModel del usuario
	 * @return boolean True -> Nombre de Usuario actualizado, False -> Falló la actualización del nombre del usuario
	 */
	public static function updateUserName_Transaction ($conn, $user) {
		$stmt = NULL;
		
		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::updateUserName);
			$stmt -> bindValue('id', $user -> get_id());
			$stmt -> bindValue('name', $user -> get_name());
			$stmt -> bindValue('surname', $user -> get_surname());
			$stmt -> bindValue('second_surname', $user -> get_second_surname());
		}
		
		return $stmt;
	}

	/**
	 * Método para actualizar el alias del usuario
	 *
	 * @param [object] $user DataModel del usuario
	 * @return boolean True -> Alias de Usuario actualizado, False -> Falló la actualización del alias del usuario
	 */
	public static function updateAlias_Transaction ($conn, $user) {
		$stmt = NULL;
		
		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::updateAlias);
			$stmt -> bindValue('id', $user -> get_id());
			$stmt -> bindValue('alias', $user -> get_alias());
		}

		return $stmt;
	}

	/**
	 * Método para actualizar la imagen del usuario, usando transacciones
	 *
	 * @param [object] $user DataModel del usuario
	 * @return stmt La sentencia sql lista para ser ejecutada
	 */
	public static function updateImageTransation ($conn, $user) {
		$stmt = NULL;

		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::updateImage);
			$stmt -> bindValue('id', $user -> get_id());
			$stmt -> bindValue('image', $user -> get_image());
		}

		return $stmt;
	}
}
