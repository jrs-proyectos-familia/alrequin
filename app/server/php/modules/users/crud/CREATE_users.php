<?php
namespace app\server\php\modules\users\crud;

use config\php\db\DB;

use app\server\php\modules\users\crud\sql\Sql as SQL_User;

class Create {
	/**
	 * Método para crear un nuevo usuario
	 *
	 * @param [object] $user DataModel del usuario
	 * @return boolean True -> Usuario creado, False -> Falló al crear el usuario
	 */
	public static function create ($user) {
		$success = false;

		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::create);
			$stmt -> bindValue('name', $user -> get_name());
			$stmt -> bindValue('surname', $user -> get_surname());
			$stmt -> bindValue('second_surname', $user -> get_secondSurname());
			$stmt -> bindValue('alias', $user -> get_alias());
			$stmt -> bindValue('image', $user -> get_image());
			
			if ($stmt -> execute()) $success = true;
		}

		return $success;
	}

	/**
	 * Método para crear un usuario usando Transacciones
	 *
	 * @param [dbConn] $conn Conexión a la base de datos
	 * @param [object] $user DataModel del usuario
	 * @return stmt La sentencia sql lista para ser ejecutada
	 */
	public static function create_Transaction ($conn, $user) {
		$stmt = NULL;

		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::create);
			
			$stmt -> bindValue('name', $user -> get_name());
			$stmt -> bindValue('surname', $user -> get_surname());
			$stmt -> bindValue('second_surname', $user -> get_second_Surname());
			$stmt -> bindValue('alias', $user -> get_alias());
			$stmt -> bindValue('image', $user -> get_image());

		}

		return $stmt;
	}
}
