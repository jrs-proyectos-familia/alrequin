<?php
namespace app\server\php\modules\users\crud;

use config\php\db\DB;

use app\server\php\modules\users\crud\sql\Sql as SQL_User;

class Delete {
	/**
	 * Método para eliminar un usuario
	 *
	 * @param [object] $user DataModel del usuario
	 * @return boolean True -> Usuario eliminado, False -> Falló la eliminación del usuario
	 */
	public static function delete ($user) {
		$success = false;

		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::delete);
			$stmt -> bindValue('id', $user -> get_id());
			
			if ($stmt -> execute()) $success = true;
		}

		return $success;
	}

	/**
	 * Método para eliminar un usuario, usando transacciones
	 *
	 * @param [object] $user DataModel del usuario
	 * @return stmt La sentencia sql lista para ser ejecutada
	 */
	public static function deleteImageTransation ($conn, $user) {
		$stmt = NULL;

		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::delete);
			$stmt -> bindValue('id', $user -> get_id());
		}

		return $stmt;
	}
}
