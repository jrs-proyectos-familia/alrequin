<?php
namespace app\server\php\modules\users\crud;

use config\php\db\DB;

use app\server\php\modules\users\crud\sql\Sql as SQL_User;
use app\server\php\modules\users\data\Data as DATA_User;

/**
 * Función que construye un objeto (Data) de la clase User con base al resultado de una busqueda en la base de datos
 *
 * @param [Result DB] $resultDB
 * @return Object User
 */
function generateUser ($resultDB) {
	$user = new DATA_user();
	
	$user -> set_id((int)$resultDB['id']);
	$user -> set_name($resultDB['name']);
	$user -> set_surname($resultDB['surname']);
	$user -> set_second_surname($resultDB['second_surname']);
	$user -> set_alias($resultDB['alias']);
	$user -> set_image($resultDB['image']);

	return $user;
}

class Read {
	 
	/**
	 * Método para obtener todos los usuarios de la base de datos
	 *
	 * @return Array Users
	 */
	public static function read () {
		$users = array();

		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> query(SQL_User::read);
			
			foreach($tmt -> fetchAll() as $row) {
				$user = generateUser($row);

				array_push($users, $user);
			}
		}

		return $users;
	}

	/**
	 * Método que encuentra un usuario con base en su alias
	 *
	 * @param [Object] $userFind Data del usuario a buscar
	 * @return Object user
	 */
	public static function read_user_by_alias ($userFind) {
		$user = null;

		$conn = DB::init() -> conn();
		
		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::readUserByAlias);
			$stmt -> bindValue('alias', $userFind -> get_alias());
			
			$stmt -> execute();

			$result = $stmt->fetch(\PDO::FETCH_ASSOC);
			
			$user = generateUser($result);
		}

		return $user;
	}

	/**
	 * Método que encuentra un usuario con base en su ID
	 *
	 * @param [Connection DB] $conn
	 * @param [Object] $userFind Data del usuario a buscar
	 * @return void
	 */
	public static function read_user_by_id_Transaction ($conn, $userFind) {
		$user = null;
		
		if (isset($conn)) {
			$stmt = $conn -> prepare(SQL_User::readUserById);
			$stmt -> bindValue('id', $userFind -> get_id());

			$stmt -> execute();

			$result = $stmt -> fetch(\PDO::FETCH_ASSOC);

			$user = generateUser($result);
		}

		return $user;
	}
}
