<?php
namespace app\server\php\modules\users\crud\sql;

class Sql {

	const create = 'INSERT INTO users (name, surname, second_surname, alias, image) VALUES(:name,:surname,:second_surname,:alias,:image)';
	
	const read = 'SELECT * FROM users ORDER BY ASC';
	const readUserByAlias = 'SELECT * FROM users WHERE alias = :alias';
	const readUserById = 'SELECT * FROM users WHERE id = :id';

	const update = 'UPDATE users SET name = :name, surname = :surname, second_surname = :second_surname, alias = :alias, image = :image WHERE id = :id';
	const updateUserName = 'UPDATE users SET name = :name, surname = :surname, second_surname = :second_surname WHERE id = :id';
	const updateAlias = 'UPDATE users SET alias = :alias WHERE id = :id';
	const updateImage = 'UPDATE users SET image = :image WHERE id = :id';

	const delete = 'DELETE FROM users WHERE id = :id';

} 
