<?php
namespace app\server\php\modules\users\rules\alias;

use app\server\php\ini\mwtools\ValRules;
use app\server\php\ini\annotations\Rules_Val;

/**
 * @Rules_Val(no_empty = true, data_type = "string", min_characters = 4, max_characters = 12)
 */
class RULE_VAL extends ValRules {
	public function __construct () {
	}
}
