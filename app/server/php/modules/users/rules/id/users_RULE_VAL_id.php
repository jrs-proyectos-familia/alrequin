<?php
namespace app\server\php\modules\users\rules\id;

use app\server\php\ini\mwtools\ValRules;
use app\server\php\ini\annotations\Rules_Val;

/**
 * @Rules_Val(no_empty = true, data_type = "integer", min_characters = -1, max_characters = -1, value_bigger = 0)
 */
class RULE_VAL extends ValRules {
	public function __construct () {
	}
}
