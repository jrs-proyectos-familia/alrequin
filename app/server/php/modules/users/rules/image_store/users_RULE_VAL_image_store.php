<?php
namespace app\server\php\modules\users\rules\image_store;

use app\server\php\ini\mwtools\ValRules;
use app\server\php\ini\annotations\Rules_Val;

/**
 * @Rules_Val(no_empty = false, data_type = "string", min_characters = 4, max_characters = 255)
 */
class RULE_VAL extends ValRules {
	public function __construct () {
	}
}
