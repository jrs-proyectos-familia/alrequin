<?php
namespace app\server\php\modules\users\rules\image_store;

use app\server\php\ini\mwtools\TreatRules;
use app\server\php\ini\annotations\Rules_Treat;

/**
 * @Rules_Treat(use_trim = true, transform_text = "skip", remove_special_characters = false)
 */
class RULE_TREAT extends TreatRules {
	public function __construct () {
	}
}
