<?php
namespace app\server\php\modules\users\rules\second_surname;

use app\server\php\ini\mwtools\TreatRules;
use app\server\php\ini\annotations\Rules_Treat;

/**
 * @Rules_Treat(use_trim = true, transform_text = "capitalize", remove_special_characters = true)
 */
class RULE_TREAT extends TreatRules {
	public function __construct () {
	}
}
