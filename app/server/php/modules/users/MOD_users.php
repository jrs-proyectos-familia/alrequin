<?php
use config\php\route\Route;
use const config\php\args\ROUTES;

use app\server\php\middleware\AuthMiddleware;
use app\server\php\modules\users\middleware\post\new_users\{
	MW_LINK as NewUserLink,
	MW_TREAT as NewUserTreat, 
	MW_VAL as NewUserVal,
	MW_SERV as NewUserServ
};
use app\server\php\modules\users\middleware\put\update_user_name\{
	MW_LINK as UpdateUserNameLink,
	MW_TREAT as UpdateUserNameTreat,
	MW_VAL as UpdateUserNameVal,
	MW_SERV as UpdateUserNameServ
};
use app\server\php\modules\users\middleware\put\update_alias\{
	MW_LINK as UpdateAliasLink,
	MW_TREAT as UpdateAliasTreat,
	MW_VAL as UpdateAliasVal,
	MW_SERV as UpdateAliasServ
};
use app\server\php\modules\users\middleware\put\update_image\{
	MW_LINK as UpdateImageLink,
	MW_TREAT as UpdateImageTreat,
	MW_VAL as UpdateImageVal, 
	MW_SERV as UpdateImageServ
};
use app\server\php\modules\users\middleware\delete\deleteUser\{TVS_TREAT as DeleteUserTreat, TVS_VAL as DeleteUserVal, TVS_SERV as DeleteUserServ};

Route::get('nombre/hola');
	
Route::get(ROUTES['USERS']['MAIN'], function($req, $res) {
	$res -> code(404, [true]);
	// $next -> mw;
	// foreach($req -> memory as $key => $VALUE) {
	// 	echo "<b>$key</b><br>";
	// 	print_r($VALUE);
	// 	echo "<br><br>";
	// }

	// $page = array(
	// 	'title' => 'Usuario'
	// );
	// $res -> render = array('view' => $req -> data['view'], 'args' => $page);
});

Route::post(ROUTES['USERS']['MAIN'],
	new NewUserLink(),
	new NewUserTreat(),
	new NewUserVal(),
	new NewUserServ()
);

Route::put(ROUTES['USERS']['EDIT_NAME'], 
	new AuthMiddleware(),
	new UpdateUserNameLink(),
	new UpdateUserNameTreat(),
	new UpdateUserNameVal(),
	new UpdateUserNameServ()
);

Route::put(ROUTES['USERS']['EDIT_ALIAS'], 
	new AuthMiddleware(),
	new UpdateAliasLink(),
	new UpdateAliasTreat(),
	new UpdateAliasVal(),
	new UpdateAliasServ()
);

Route::post(ROUTES['USERS']['EDIT_IMAGE'], 
	new AuthMiddleware(), 
	new UpdateImageLink(),
	new UpdateImageTreat(), 
	new UpdateImageVal(), 
	new UpdateImageServ()
);

// Route::delete(View::route['main'], new DeleteUserTreat(), new DeleteUserVal(), new DeleteUserServ());
Route::delete(ROUTES['USERS']['MAIN'], function($req, $res) {
	echo "LISTO PARA BORRAR";
});
