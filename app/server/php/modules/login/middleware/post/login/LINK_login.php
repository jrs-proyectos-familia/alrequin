<?php
namespace app\server\php\modules\login\middleware\post\login;

use app\server\php\tools\DataManage;

trait Link_Rules {
	public function Rule_Alias () {
		// Val
		$this -> linkChest['val']['rules']['alias'] -> val_exists(array('withID' => -1));
	}
}

class MW_LINK {

	use Link_Rules;

	const validData = array(
		'alias'
	);

	private $reqData = NULL;
	private $linkChest = NULL;

	public function middleware ($req, $res, $next) {
		$this -> reqData = $req -> memory;

		$this -> linkChest = DataManage::linkData($this -> reqData['view'], $this -> reqData['data'], self::validData);

		if (count($this -> linkChest['treat']['data']) === count(self::validData)) {
			// 						=== UPDATE RULES ===
			$this -> Rule_Alias();
			
			$req -> set_merge($this -> linkChest);
			
			$next -> mw;
		} else $res -> code(406, ['error' => 'Hubo un error con los datos recibidos :(']);
	}
}
