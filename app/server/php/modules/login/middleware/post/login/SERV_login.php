<?php
namespace app\server\php\modules\login\middleware\post\login;

use config\php\auth\Auth;
use config\php\exceptions\AuthException;

use app\server\php\modules\users\crud\Read as CRUD_Read;

class MW_SERV {

	private $reqData = NULL;
	private $model = NULL;

	private $session = array(
		'user' => NULL,
		'token' => NULL
	);

	public function middleware ($req, $res) {
		$this -> reqData = $req -> memory;

		$this -> model = $this -> reqData['serv']['model'];
		
		$this -> session['user'] = CRUD_Read::read_user_by_alias($this -> model);
		
		$this -> session['token'] = Auth::generate($this -> session['user']);
		
		if ($this -> session['token'] !== NULL)
			$res -> code(201, [
				'token' => $this -> session['token'],
				'user' => array(
					'name' => $this -> session['user'] -> get_name(),
					'surname' => $this -> session['user'] -> get_surname(),
					'second_surname' => $this -> session['user'] -> get_second_surname(),
					'alias' => $this -> session['user'] -> get_alias(),
					'image' => $this -> session['user'] -> get_image()
				)
			]);
		else throw new AuthException("ATE-100", "Couldn't generate token");
	}
}
