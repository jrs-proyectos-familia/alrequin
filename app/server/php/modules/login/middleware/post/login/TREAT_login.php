<?php
namespace app\server\php\modules\login\middleware\post\login;

use app\server\php\mixins\Treat;

class MW_TREAT {
	public function middleware ($req, $res, $next) {
		$reqData = $req -> memory;

		if (count($reqData['treat']['data'])) {
			$Treat = new Treat($reqData['treat']);

			$trunkDataTreat = $Treat -> treatment();

			$req -> set_key('treat\data', $trunkDataTreat);

			$next -> mw;
		} else $res -> code(500, ['error' => 'No pasó el tratamiento de los datos']);
	}
}
