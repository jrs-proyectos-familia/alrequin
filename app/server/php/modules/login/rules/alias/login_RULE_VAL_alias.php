<?php
namespace app\server\php\modules\login\rules\alias;

use app\server\php\modules\users\rules\alias\RULE_VAL as RULE_ALIAS;
use app\server\php\ini\annotations\Rules_Val;

/**
 * @Rules_Val(clone = "app\server\php\modules\users\rules\alias\RULE_VAL")
 */
class RULE_VAL extends RULE_ALIAS {
	public function __construct () {
		parent::__construct();
	}
}
