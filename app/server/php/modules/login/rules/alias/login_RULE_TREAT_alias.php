<?php
namespace app\server\php\modules\login\rules\alias;

use app\server\php\modules\users\rules\alias\RULE_TREAT as RULE_ALIAS;
use app\server\php\ini\annotations\Rules_Treat;

/**
 * @Rules_Treat(clone = "app\server\php\modules\users\rules\alias\RULE_TREAT")
 */
class RULE_TREAT extends RULE_ALIAS {
	public function __construct () {
		parent::__construct();
	}
}
