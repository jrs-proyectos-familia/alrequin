<?php
use config\php\route\Route;
use const config\php\args\ROUTES;

use app\server\php\middleware\AuthMiddleware;
use app\server\php\modules\login\middleware\post\login\{
	MW_LINK as LoginLink,
	MW_TREAT as LoginTreat,
	MW_VAL as LoginVal,
	MW_SERV as LoginServ
};

Route::post(ROUTES['LOGIN']['MAIN'],
	new LoginLink(),
	new LoginTreat(),
	new LoginVal(),
	new LoginServ()
);

// Route::post(ROUTES['LOGIN']['CHECK'], new AuthMiddleware(), function ($req, $res) {
// 	$res -> code(200);
// });
