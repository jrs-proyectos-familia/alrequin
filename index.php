<?php
require_once('config/php/charger/CONF_charger.php');

use config\php\charger\{ConfigCharger, AppCharger};
use config\php\route\{Route, Redirect};
use const config\php\args\ROUTES;

use config\php\exceptions\ErrorsCatch;

try {
	new AppCharger();
	new ConfigCharger();

	if (!isset($_GET['url'])) Redirect::route(ROUTES['HOME']);
	else Route::lauchRoute($_GET['url']);
} catch (\Exception $e) {
	ErrorsCatch::init() -> catchException($e);
} finally {
	ErrorsCatch::init() -> finallException();
	// Moverlo en las operaciones necesarias
	// try {
	// 	DB::init() -> conn() -> rollBack();
	// } catch (\Exception $e2) {
	// }

	// DB::destroy();
	
	
}

