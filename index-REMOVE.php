<?php
require_once('vendor/autoload.php');
require_once('doctrine/anotaciones/anotacionSaludar.php');
require_once('doctrine/anotaciones/anotacionDespedida.php');
require_once('doctrine/ejemplos/saludando.php');
require_once('doctrine/config/lectorAnotaciones.php');

use doctrine\config\Lector;

$reflection = new \ReflectionClass('doctrine\a\Saludando');
$propElSaludo = $reflection;
// $anotacion = Lector::anotacion($propElSaludo, 'doctrine\b\Anotacion');
$anotacion = Lector::anotacion($propElSaludo, 'doctrine\a\Anotacion');

print_r($anotacion);
