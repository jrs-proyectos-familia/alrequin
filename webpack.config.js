const path = require('path');

require('dotenv').config({
	path: './config/.env'
})

const { WebpackArgs } = require('./config/webpack/WEBPACK_args')(process.env)
const { WebpackTools } = require('./config/webpack/WEBPACK_tools')
const { WebpackOptimization } = require('./config/webpack/WEBPACK_optimization')
const { WebpackEntries } = require('./config/webpack/WEBPACK_entries')
const { WebpackAlias } = require('./config/webpack/WEBPACK_alias')
const { WebpackModule } = require('./config/webpack/WEBPACK_modules')
const { WebpackPlugins } = require('./config/webpack/WEBPACK_plugins')

WebpackTools.removePublic(WebpackArgs.publicDir)

// Configuración principal de webpack
const webpackConfig = {
	// Modo de desarrollo o producción
	mode: process.env.MODE === 'prod' ? 'production' : 'development',

	// Modo de observación para compilar cambios realizados en el frontend (modo dev)
	watch: process.env.MODE === 'dev' ? true : false,

	// Optimización de los archivos resultantes para hacer ligeros los archivos JS
	optimization: WebpackOptimization.optimization(),

	// Alias disponibles en el apartado Client (Frontend)
	resolve: WebpackAlias.resolve(),

	// Entrada de los archivos principales JS y su correspondiente salida
	entry: WebpackEntries.entriesJS,
	output: WebpackEntries.output(process.env, WebpackArgs),

	// Apartado de modulos (plugins webpack)
	module: WebpackModule.module(process.env, WebpackArgs),

	// Necesario para pasar datos de webpack server a plantillas Twig ("Ïndispensable cuando se trabaja en Dev en modo dev")
	node: {
		fs: 'empty'
	},

	// Apartado de Plugins
	plugins: WebpackPlugins.plugins(process.env, Object.keys(WebpackEntries.entriesJS), WebpackArgs)
}

module.exports = webpackConfig;
